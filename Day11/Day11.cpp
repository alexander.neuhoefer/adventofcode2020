#include <iostream>
#include <fstream>
#include <map>
#include <set>
#include <vector>
#include <string>
#include <regex>

struct State
{
    std::vector<char> data = {};
    size_t width = 0;
    size_t height = 0;
};

void printState(const std::string& label, const State& state)
{
    auto start = state.data.cbegin();
    std::cout << label << std::endl;
    for (size_t y = 0; y < state.height; ++y)
    {
        auto end = start + state.width;
        std::cout << std::string(start, end) << std::endl;
        start = end;
    }
    std::cout << std::string(state.width, '_') << std::endl;
}

size_t countOccupiedSeats(const State& state, const int x0, const int y0)
{
    size_t count = 0;
    const std::vector<std::pair<int, int>> dir{
        {-1, -1}, {0, -1}, {1, -1},
        {-1,  0},          {1,  0},
        {-1,  1}, {0,  1}, {1,  1},
    };

    for(const auto& d : dir)
    {
        int x = x0 + d.first;
        int y = y0 + d.second;
        while (x >= 0 && x < state.width
            && y >= 0 && y < state.height)
        {
            const auto& s = state.data.at(x + y * state.width);
            if (s == '#')
            {
                ++count;
                break;
            }
            if (s == 'L')
            {
                break;
            }
            x += d.first;
            y += d.second;
        }
    }
    return count;
}

bool update(State& originalState, State& tmpState)
{
    size_t i = 0;
    for (size_t y = 0; y < originalState.height; ++y)
    {
        for (size_t x = 0; x < originalState.width; ++x, ++i)
        {
            const auto occupied = countOccupiedSeats(originalState, x, y);
            const auto& src = originalState.data.at(i);
            auto& dst = tmpState.data.at(i);
            if (src == 'L' && occupied == 0)
            {
                dst = '#';
            }
            else if (src == '#' && occupied >= 5)
            {
                dst = 'L';
            }
            else
            {
                dst = src;
            }
        }
    }

    std::swap(originalState, tmpState);
    printState("Update:", originalState);
    return !std::equal(originalState.data.cbegin(), originalState.data.cend(), tmpState.data.cbegin());
}

int main()
{
    auto inputFile = std::ifstream("Input.txt");

    std::string line;
    State state;
    while (std::getline(inputFile, line))
    {
        if (!state.width)
        {
            state.width = line.size();
        }
        ++state.height;
        state.data.insert(state.data.end(), line.cbegin(), line.cend());
    }
    printState("Initial State:", state);

    auto tmpState = state;
    do{} while(update(state, tmpState));
    std::cout << std::endl << "Occupied seats: " << std::count(state.data.cbegin(), state.data.cend(), '#');
}