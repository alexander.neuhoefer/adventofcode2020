#include <iostream>
#include <fstream>
#include <vector>
#include <string>

std::vector<bool> convert(const std::string& line)
{
    std::vector<bool> map(line.size());
    auto it = map.begin();
    for (const auto& c : line)
    {
        *it++ = c == '#' ? true : false;
    }
    return map;
}

int main()
{
    auto inputFile = std::ifstream("C:\\Users\\alexa\\source\\repos\\Day3\\Input.txt");
    std::vector<std::vector<bool>> map;

    std::string line;
    while (std::getline(inputFile, line))
    {
        std::cout << "_" << line << std::endl;
        map.push_back(convert(line));
    }

    size_t answer = 1;
    for (const auto& entry : std::vector<std::pair<size_t,size_t>>{ {1, 1}, {3, 1}, {5, 1}, {7, 1}, {1, 2} })
    {
        size_t x = 0, y = 0, width = map[0].size();
        const size_t& sx = entry.first, sy = entry.second;
        size_t encounteredTrees = 0;
        while (y < map.size())
        {
            if (map[y][x])
            {
                ++encounteredTrees;
            }
            x = (x + sx) % width;
            y += sy;
        }
        std::cout << encounteredTrees << std::endl;
        answer *= encounteredTrees;
    }
    std::cout << answer;
}