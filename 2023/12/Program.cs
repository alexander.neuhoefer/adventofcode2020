﻿var input = File.ReadAllLines("./shortInput.txt")
    .Select(line => new Info(line.Split(' ')))
    .ToList();

Console.WriteLine($"{string.Join('\n', input)}");
Console.WriteLine($"Sum of potential arrangements: {input.Select(i => i.Arangements).Sum()}");

record Info(string[] Line)
{
    public string Conditions = string.Join('?',
            Enumerable.Repeat(Line[0], 5)
            .SelectMany(s => s)
        );

    private int[] counts = Enumerable.Repeat(Line[1].Split(','), 5)
        .SelectMany(s => s.Select(i => int.Parse(i)))
        .ToArray();

    public string Counts => string.Join(',', counts);

    private string[] groups(string line) => line.Split('.')
        .Where(s => !string.IsNullOrWhiteSpace(s))
        .ToArray();

    private int NumberOfPlaceholders => Conditions.Count(c => c == '?');

    public int CalcArangements()
    {
        var limit = Math.Pow(2, NumberOfPlaceholders);
        myValidArangements = 0;
        var maxSet = (ulong)(counts.Sum() - Conditions.Count(c => c == '#'));

        for (ulong variant = 0; variant < limit; ++variant)
        {
            if (System.Runtime.Intrinsics.X86.Popcnt.X64.PopCount(variant) > maxSet)
                continue;

            var line = Conditions.ToCharArray();
            int bit = 0;
            for (int i = 0; i < line.Length; ++i)
            {
                if (line[i] == '?')
                {
                    line[i] = (variant & (1ul << bit)) == 0 ? '#' : '.';
                    ++bit;
                }
            }
            var valid = IsValidArrangement(new string(line));
            Console.WriteLine($"{new string(line)}, valid under counts {Counts}: {valid}");
            myValidArangements += valid ? 1 : 0;
        }
        return myValidArangements;
    }

    public int Arangements => myValidArangements < 0 ? CalcArangements() : myValidArangements;
    private int myValidArangements = -1;

    private bool IsValidArrangement(string line)
    {
        var parts = groups(line);
        return parts.Count() == counts.Count() && parts.Zip(counts)
            .All(p => p.First.Count() == p.Second);
    }
}