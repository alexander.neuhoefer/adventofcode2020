#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <string>

int main()
{
    auto file = std::ifstream("Input.txt");
    std::vector<int> calories{0};
    calories.reserve(256);

    std::string line;
    while (std::getline(file, line))
    {
        if(line.empty())
        {
            calories.emplace_back(0);
        }
        else
        {
            calories.back() += std::stoi(line);
        }
    }

    const auto maxCalories = std::ranges::max_element(calories);
    std::cout << "max calories: " << *maxCalories << std::endl;

    std::ranges::partial_sort(calories, calories.begin() + 3, std::greater<int>());
    const auto topThreeSum = std::reduce(calories.begin(), calories.begin() + 3);
    std::cout << "sum over the top three calories: " << topThreeSum << std::endl;
}