#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include <vector>

static size_t findMatchingClosingBracket(const std::string& text)
{
    int depth = 1;
    for(int i = 1; i < text.size(); ++i)
    {
        if(text[i] == '[') depth += 1;
        if(text[i] == ']') depth -= 1;
        if(depth == 0)
        {
            return i;
        }
    }
    return 0;
}

bool isInteger(const std::string& str)
{
    return str[0] - '0' < 10;
}

static int compareLists(const std::string& a, const std::string& b)
{
    //std::cout << "   comparing: " << a << " with " << b << std::endl;
    if(a.empty() || b.empty())
    {
        return b.empty() - a.empty();
    }
    if(a[0] == ',' && b[0] == ',')
    {
        return compareLists(a.substr(1), b.substr(1));
    }

    if(isInteger(a) && isInteger(b))
    {
        int ia = std::stoi(a.c_str());
        int ib = std::stoi(b.c_str());
        if(ia < ib)
        {
            return -1;
        }
        if(ia > ib)
        {
            return 1;
        }
        return compareLists(a.substr(ia / 10 + 1), b.substr(ib / 10 + 1));
    }

    if(a[0] == '[' && b[0] == '[')
    {
        auto closingBracketA = findMatchingClosingBracket(a);
        auto closingBracketB = findMatchingClosingBracket(b);
        int intermediateResult = compareLists(a.substr(1, closingBracketA - 1), b.substr(1, closingBracketB - 1));
        if(intermediateResult == 0)
        {
            return compareLists(a.substr(closingBracketA + 1), b.substr(closingBracketB + 1));
        }
        return intermediateResult;
    }

    if(a[0] == '[')
    {
        int digits = std::atoi(b.c_str()) / 10 + 1;
        return compareLists(a, "[" + b.substr(0,digits) + "]" + b.substr(digits));
    }
    if(b[0] == '[')
    {
        int digits = std::atoi(a.c_str()) / 10 + 1;
        return compareLists("[" + a.substr(0,digits) + "]" + a.substr(digits), b);
    }
}

int main()
{
    auto file = std::ifstream("Input.txt");

    std::string a;
    std::vector<std::string> packets;
    while(file >> a)
    {
        packets.push_back(a);
    }

    std::sort(packets.begin(), packets.end(), [](const auto& a, const auto&b){return compareLists(a,b) <= 0;});

    int i = 0, decoderKey;
    for(const auto& s : packets)
    {
        ++i;
        if(s == "[[2]]") decoderKey = i;
        if(s == "[[6]]") decoderKey *= i;
        std::cout << s << std::endl;
    }

    std::cout << "decoderKey: " << decoderKey << std::endl;

    return 0;
}