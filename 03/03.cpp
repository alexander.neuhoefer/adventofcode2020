#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <vector>
#include <bitset>

constexpr std::size_t LENGTH_OF_INPUT = 12;
using Number = std::bitset<LENGTH_OF_INPUT>;

//int main()
//{
//    auto inputFile = std::ifstream("Input.txt");
//
//    std::string binaryString;
//    std::vector<uint16_t> countOfOnes(LENGTH_OF_INPUT, 0);
//    int numberOfValues = 0;
//    while (!(inputFile >> binaryString).eof())
//    {
//        std::cout << binaryString << "\t";
//
//        std::cout << "Updating number of ones: ";
//        auto idx = std::begin(countOfOnes);
//        for (const auto c : binaryString)
//        {
//            *idx += c - '0';
//            std::cout << *idx << ",";
//            ++idx;
//        }
//        ++numberOfValues;
//        std::cout << std::endl;
//    }
//
//    std::bitset<LENGTH_OF_INPUT> gammaRate;
//    const auto halfSize = numberOfValues / 2;
//    std::cout << "halfSize: " << halfSize << std::endl;
//    for (int i = 0; i < LENGTH_OF_INPUT; ++i)
//    {
//        int invertedIndex = LENGTH_OF_INPUT - 1 - i;
//        gammaRate[i] = countOfOnes[invertedIndex] > halfSize;
//    }
//
//    std::cout << "gamma rate: " << gammaRate.to_string() << " as integer: " << gammaRate.to_ulong() << std::endl;
//    auto epsilonRate = gammaRate;
//    epsilonRate.flip();
//    std::cout << "epsilon rate: " << epsilonRate << " as integer: " << epsilonRate.to_ulong() << std::endl;
//    std::cout << "result: " << gammaRate.to_ulong() * epsilonRate.to_ulong() << std::endl;
//}

template<class It>
size_t countOnesAtPosition(
    It from, It to,
    int i)
{
    return std::count_if(from, to, [&](const Number& n) {
        return n[LENGTH_OF_INPUT - 1 - i];
    });
}

template<int Increment, class It> Number
filter(
    It from, It to,
    int i)
{
    size_t remainingValues = to - from;
    if (remainingValues == 1) return *from;

    size_t numberOfOnes = countOnesAtPosition(from, to, i);
    bool valueToKeep = Increment > 0
        ? numberOfOnes >= ((remainingValues + 1) / 2)
        : numberOfOnes < ((remainingValues + 1) / 2);
    std::cout << "Got " << numberOfOnes << " ones with " << remainingValues << " values remaining, so looking for: " << valueToKeep << " at index " << i << std::endl;

    auto middle = std::remove_if(from, to, [=](const Number& n) {
        return n[LENGTH_OF_INPUT - 1 - i] != valueToKeep;
    });
    //std::cout << "left: ";
    //for (auto it = from; it != middle; ++it)
    //{
    //    std::cout << *it << ",";
    //}
    //std::cout << std::endl;

    return filter<Increment>(from, middle, i + 1);
}

int main()
{
    auto inputFile = std::ifstream("Input.txt");

    Number number;
    std::vector<Number> input;
    while (!(inputFile >> number).eof())
    {
        input.push_back(number);
    }

    auto copy = input;
    const auto oxygenGeneratorRating = filter<1>(std::begin(copy), std::end(copy), 0);
    std::cout << "oxygenGeneratorRating: " << oxygenGeneratorRating << " as number: " << oxygenGeneratorRating.to_ulong() << std::endl;

    const auto co2ScrubberRating = filter<-1>(std::begin(input), std::end(input), 0);
    std::cout << "co2ScrubberRating: " << co2ScrubberRating << " as number: " << co2ScrubberRating.to_ulong() << std::endl;

    std::cout << "result: " << oxygenGeneratorRating.to_ulong() * co2ScrubberRating.to_ulong() << std::endl;
}