﻿var availableColors = new Dictionary<string,int>
{
    {"red", 12}, {"green", 13}, {"blue", 14},
};

(string, int) ToKvp(string s)
{
    var parts = s.Trim().Split(' ');
    return (parts[1], int.Parse(parts[0]));
};

bool IsPossible(string drawnNumbers) => drawnNumbers.Trim().Split(',').All(s =>
{
    var (key, count) = ToKvp(s);
    return count <= availableColors[key];
});

int sum = 0;
int powerSum = 0;
int gameId = 0;
foreach (var line in File.ReadLines("./longInput.txt"))
{
    var drawnNumbers = line.Split(':')[1].Split(';');
    ++gameId;
    if(drawnNumbers.All(IsPossible))
    {
        sum += gameId;
        Console.WriteLine($"Game {gameId} was possible");
    }
    powerSum += ToPower(drawnNumbers);
    Console.WriteLine($"running sum: {sum}, powerSum: {powerSum}");
}

//part 2
int ToPower(string[] drawnNumbers)
{
    var drawnCounts = drawnNumbers.SelectMany(s => s.Trim().Split(','));
    var maxCounts = new Dictionary<string, int>();
    foreach (var s in drawnCounts)
    {
        var (key, count) = ToKvp(s);

        if (!maxCounts.ContainsKey(key) || maxCounts[key] < count)
        {
            maxCounts[key] = count;
        }
    }
    return maxCounts.Aggregate(1, (product, kvp) => product * kvp.Value);
};