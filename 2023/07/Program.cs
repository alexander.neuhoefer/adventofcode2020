﻿var input = File.ReadAllLines("./longInput.txt")
    .Select(i => new Game(i))
    .Order().ToList();

var winnings = 0;
var i = 0;
foreach (var g in input)
{
    winnings += g.Bid * ++i;
    Console.WriteLine($"{g.Hand} {g.Bid} {g.Type} => {winnings}");
}

public class Game : IComparable<Game>
{
    public Game(string input)
    {
        var tmp = input.Split(' ');
        (Hand, Bid) = (tmp[0], int.Parse(tmp[1]));
        Type = DetermineType();
    }

    public string Hand { get; set; }
    public int Bid { get; set; }
    public int Type;

    public int CompareTo(Game other)
    {
        if (Type != other.Type) return Type.CompareTo(other.Type);

        var pair = Hand.Zip(other.Hand).FirstOrDefault(p => p.First != p.Second, new ('2', '2'));
        return Rank(pair.First).CompareTo(Rank(pair.Second));
    }

    private static List<char> AvailableCards = "AKQT98765432J".Reverse().ToList();
    private int Rank(char c) => AvailableCards.IndexOf(c);

    private int DetermineType()
    {
        var countOfCards = AvailableCards.ToDictionary(c => c, _ => 0);
        foreach (var c in Hand)
        {
            ++countOfCards[c];
        }

        for(int i = countOfCards['J']; i > 0; --i)
        {
            //Replace one joker with most common card not being a joker
            var mostCommonCard = countOfCards.Where(kvp => kvp.Key != 'J')
                .OrderBy(kvp => kvp.Value).Last().Key;
            ++countOfCards[mostCommonCard];
            --countOfCards['J'];
        }

        var values = countOfCards.Values;
        return values.Max() switch
        {
            5 => 6, // five of a kind
            4 => 5, // four of a kind
            3 => values.Contains(2) ? 4 : 3, // full house / three of a kind
            2 => values.Count(v => v == 2), // two pairs / pair
            _ => 0 // high card
        };
    }
}