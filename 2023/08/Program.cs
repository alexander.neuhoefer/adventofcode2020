﻿using System.Collections.Frozen;

var input = File.ReadAllLines("./longInput.txt");
var directions = input.First();

var ExtractLocation = (string s) => s.Split(' ').First().Trim();

var ExtractDirections = (string s) => {
    var parts = s.Split([' ', ',', '(', ')']);
    return (parts[3], parts[5]);
};

var map = input.Skip(2).ToFrozenDictionary(ExtractLocation, ExtractDirections);

string[] locations = "RCS,FPF,KLM,JSJ,TVK,XPX".Split(',').ToArray();//map.Keys.Where(k => k.EndsWith('A')).ToArray();
ulong step = 1962960000000;
ulong length = (ulong)directions.Length;
//2259190000000: Moving towards L to XQV,BFN,DXJ,FKC,NXG,TRB

while (locations.Any(s => s[2] != 'Z'))
{
    var dir = directions[(int)(step++ % length)];
    //locations = (dir == 'L'
    //    ? locations.Select((string s) => map[s].Item1)
    //    : locations.Select((string s) => map[s].Item2)
    //    ).ToArray();
    if (dir == 'L')
    {
        locations[0] = map[locations[0]].Item1;
        locations[1] = map[locations[1]].Item1;
        locations[2] = map[locations[2]].Item1;
        locations[3] = map[locations[3]].Item1;
        locations[4] = map[locations[4]].Item1;
        locations[5] = map[locations[5]].Item1;
    }
    else
    {
        locations[0] = map[locations[0]].Item2;
        locations[1] = map[locations[1]].Item2;
        locations[2] = map[locations[2]].Item2;
        locations[3] = map[locations[3]].Item2;
        locations[4] = map[locations[4]].Item2;
        locations[5] = map[locations[5]].Item2;
    }
    if (step % 10000000 == 0)
        Console.WriteLine($"{step}: Moving towards {dir} to {string.Join(',', locations)}");
}
Console.WriteLine($"{step}: {string.Join(',', locations)}");