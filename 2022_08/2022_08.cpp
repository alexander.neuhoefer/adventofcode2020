#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <string>
#include <vector>

template<typename InputIt, typename Condition>
static size_t count_until(InputIt begin, InputIt end, Condition cond)
{
    size_t count{ 0 };
    for(auto&& it = begin; it < end; ++it)
    {
        ++count;
        if(cond(*it))
        {
            break;
        }
    }
    return count;
}

template<typename InputIt, typename Condition>
static size_t count_until_r(InputIt begin, InputIt end, Condition cond)
{
    size_t count{ 0 };
    for(auto&& it = --end; it >= begin; --it)
    {
        ++count;
        if(cond(*it))
        {
            break;
        }
    }
    return count;
}

int main()
{
    auto file = std::ifstream("Input.txt");

    constexpr size_t MARKER_SIZE = 14;
    std::string line;
    std::vector<std::vector<char>> grid;
    while(std::getline(file, line))
    {
        grid.emplace_back(line.size(), 0);
        std::transform(line.cbegin(), line.cend(), grid.back().begin(), [](auto&& c)
        {
            return c - '0';
        });
    }

    size_t visible = grid.size() * 2 + grid.front().size() * 2 - 4;
    size_t maxScenicScore = 0;
    for(int y = 1; y < grid.size() - 1; ++y)
    {
        const auto& line = grid[y];
        for(int x = 1; x < line.size() - 1; ++x)
        {
            auto c = line[x];
            visible += static_cast<size_t>(
                   std::all_of(line.cbegin(), line.cbegin() + x, [=](auto&& a){return a < c;})
                || std::all_of(line.cbegin() + x + 1, line.cend(), [=](auto&& a){return a < c;})
                || std::all_of(grid.cbegin(), grid.cbegin() + y, [=](auto&& a){return a[x] < c;})
                || std::all_of(grid.cbegin() + y + 1, grid.cend(), [=](auto&& a){return a[x] < c;})
            );
            maxScenicScore = std::max<size_t>(maxScenicScore,
                  count_until_r(line.cbegin(), line.cbegin() + x, [=](auto&& a){return a >= c;})
                * count_until(line.cbegin() + x + 1, line.cend(), [=](auto&& a){return a >= c;})
                * count_until_r(grid.cbegin(), grid.cbegin() + y, [=](auto&& a){return a[x] >= c;})
                * count_until(grid.cbegin() + y + 1, grid.cend(), [=](auto&& a){return a[x] >= c;})
            );
        }
    }

    std::cout << "visible trees: " << visible << ", maxScenicScore: " << maxScenicScore << std::endl;
}