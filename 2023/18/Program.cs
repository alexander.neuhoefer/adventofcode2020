﻿using System.Numerics;

var input = File.ReadAllLines("./shortInput.txt")
    .Select(line => new Instruction(line))
    .ToList();

var offset = new Dictionary<char, Point>
{
    ['U'] = new(0, -1),
    ['D'] = new(0, 1),
    ['L'] = new(-1, 0),
    ['R'] = new(1, 0)
};

var p = new Point(0, 0);
int borderLength = 0;
var transitions = new Dictionary<int, List<int>>();

foreach (var instruction in input)
{
    var length = instruction.Length;
    var (start, end) = (p, p.Add(offset[instruction.Direction].Mul(length)));

    //if(start.X != end.X)
    {
        borderLength += length;
    }

    p = end;
    Console.WriteLine($"{instruction}, borderLength={borderLength} => {p}");

    if(start.Y != end.Y)
    {
        var x = start.X;
        var o = end.Y > start.Y ? 1 : -1;
        for(int y = start.Y; y != end.Y + o; y += o)
        {
            if (!transitions.ContainsKey(y))
            {
                transitions.Add(y, []);
            }
            transitions[y].Add(x);
        }
    }
    else
    {
        var y = start.Y;
        if (!transitions.ContainsKey(y))
        {
            transitions.Add(y, []);
        }
        //var o = end.X > start.X ? 1 : -1;
        transitions[y].AddRange([start.X, end.X]);
    }
}

BigInteger innerPoints = 0;
foreach (var transitionList in transitions.Values)
{
    transitionList.Sort();
    var tmp = transitionList;
    if(tmp.Count % 2 != 0)
    {
        tmp = transitionList.Distinct().ToList();
        Console.WriteLine($"{string.Join(',', transitionList)} -> {string.Join(',', tmp)}");
    }
    for (int i = 1; i < tmp.Count(); i += 2)
    {
        innerPoints += tmp[i] - tmp[i - 1] - 2;
    }
}

Console.WriteLine($"Cubic meters of lava is {borderLength} + {innerPoints} = {borderLength + innerPoints}, off by {borderLength + innerPoints - 952408144115}");

record Instruction(string Line)
{
    private static readonly Dictionary<char,char> NumberToDirection = "RDLU".Zip("0123").ToDictionary(kvp => kvp.Second, kvp => kvp.First);
    public char Direction => NumberToDirection[Code.Last()];
    public int Length => Convert.ToInt32("0x" + Code.AsSpan(1, 5).ToString(), 16);
    private readonly char[] Code = Line.Split(' ')[2].AsSpan(1, 7).ToArray();
};

record Point(int X, int Y)
{
    public Point Add(Point o) => new (X + o.X, Y + o.Y);
    public Point Mul(int s) => new(s * X, s * Y);
};