﻿var patterns = File.ReadAllLines("./longInput.txt").ToList();

bool IsMostlyIdentical(IEnumerable<char> a, IEnumerable<char> b)
{
    //use 0 instead of 1 to get back to have a solution for part 1
    return a.Zip(b).Count(p => p.First != p.Second) == 1;
}

IEnumerable<int> horizontalMirrorLines(string[] pattern)
{
    for (int y = 1; y < pattern.Length; ++y)
    {
        var start = Math.Max(0, y + y - pattern.Length);
        var end = Math.Min(pattern.Length, y + y);

        var a = pattern[start..y].Reverse().SelectMany(s => s);
        var b = pattern[y..end].SelectMany(s => s);

        if (IsMostlyIdentical(a, b))
            yield return y;
    }
}

IEnumerable<int> verticalMirrorLines(string[] pattern)
{
    for (int x = 1; x < pattern[0].Length; ++x)
    {
        var start = Math.Max(0, x + x - pattern[0].Length);
        var end = Math.Min(pattern[0].Length, x + x);

        var a = pattern.SelectMany(s => s[start..x].Reverse());
        var b = pattern.SelectMany(s => s[x..end]);

        if (IsMostlyIdentical(a, b))
            yield return x;
    }
}

var sum = 0;
while(patterns.Any())
{
    var current = patterns.TakeWhile(s => !string.IsNullOrEmpty(s)).ToArray();
    patterns = patterns.Skip(current.Count() + 1).ToList();

    var horizontal = horizontalMirrorLines(current).ToList();
    var vertical = verticalMirrorLines(current).ToList();
    sum += horizontal.Sum() * 100 + vertical.Sum();

    Console.WriteLine($"{string.Join('\n', current)}, mirrors at h=[{string.Join(',', horizontal)}], v=[{string.Join(',', vertical)}]");
    Console.WriteLine($"{new string('-', 10)} running sum is {sum}");
}