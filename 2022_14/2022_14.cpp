#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

constexpr bool PART_1 = false;
constexpr int SIZE = 401UL;

struct Point2D {
    int x, y;

    auto operator<=>(const Point2D&) const = default;

    auto& operator+=(const Point2D& rhs)
    {
        *this = { x + rhs.x, y + rhs.y };
        return *this;
    }
};

std::istream& operator>>(std::istream& stream, Point2D& p)
{
    char _;
    return stream >> p.x >> _ >> p.y;
}

std::ostream& operator<<(std::ostream& stream, const Point2D& p)
{
    return stream << p.x << "," << p.y;
}

constexpr Point2D SPAWN{ 500, 0 };
using Grid = std::vector<std::string>;

enum GridType : uint8_t
{
    ROCK = '#',
    BACKGROUND = '.',
    SOURCE = 'S',
    SAND = 'o',
    FLOOR = '_', //used in part 1 to terminate the loop if reached
};

Point2D toLocalPoint2D(const Point2D& p)
{
    return { p.x - SPAWN.x + SIZE / 2, p.y };
}

auto& access(Grid& grid, const Point2D& p)
{
    const auto local = toLocalPoint2D(p);
    return grid.at(local.y).at(local.x);
}

void drawLine(Grid& grid, const Point2D& a, const Point2D& b)
{
    const int maxX = std::max(a.x, b.x);
    for (int x = std::min(a.x, b.x); x <= maxX; ++x)
    {
        access(grid, { x, a.y }) = GridType::ROCK;
    }
    const int maxY = std::max(a.y, b.y);
    for (int y = std::min(a.y, b.y); y <= maxY; ++y)
    {
        access(grid, { a.x, y }) = GridType::ROCK;
    }
}

std::ostream& operator<<(std::ostream& stream, const Grid& grid)
{
    for (const auto& line : grid)
    {
        stream << line << std::endl;
    }
    return stream;
}

int main()
{
    auto file = std::ifstream("Input.txt");

    Grid grid{ SIZE, std::string(static_cast<size_t>(SIZE), GridType::BACKGROUND) };
    std::string line;
    Point2D a, b;
    int maxY = 0;
    while (getline(file, line))
    {
        std::stringstream stream(line);
        stream >> a;
        maxY = std::max(maxY, a.y);
        while(stream.ignore(sizeof(" ->")))
        {
            stream >> b;
            maxY = std::max(maxY, b.y);
            drawLine(grid, a, b);
            a = b;
        }
    }

    access(grid, SPAWN) = GridType::SOURCE;
    grid.resize(maxY + 2 + 1);
    grid.back() = std::string(static_cast<size_t>(SIZE), PART_1 ? GridType::FLOOR : GridType::ROCK);
    std::cout << grid << std::endl;

    bool finished = false;
    int unitsOfSand = 0;
    while (!finished && access(grid, SPAWN) != GridType::SAND)
    {
        unitsOfSand += 1;
        Point2D sand = SPAWN;
        while(true)
        {
            if (access(grid, sand += { 0, 1 }) == GridType::FLOOR)
            {
                finished = true;
                unitsOfSand -= 1;
                break;
            }

            if (access(grid, sand) == GridType::BACKGROUND
                || access(grid, sand += { -1, 0 }) == GridType::BACKGROUND
                || access(grid, sand += {  2, 0 }) == GridType::BACKGROUND)
            {
                continue;
            }

            access(grid, sand += { -1, -1 }) = GridType::SAND;
            break;
        }
    }
    std::cout << grid << std::endl;
    std::cout << "Units of sand dropped: " << unitsOfSand << std::endl;
}