#include <iostream>
#include <fstream>
#include <vector>

bool isValid(int min, int max, char c, const std::string& password)
{
    auto occurences = std::count(password.cbegin(), password.cend(), c);
    return occurences >= min && occurences <= max;
}

bool isValid2(int a, int b, char c, const std::string& password)
{
    return (password[a-1] == c) ^ (password[b-1] == c);
}


int main()
{
    auto inputFile = std::ifstream("C:\\Users\\alexa\\source\\repos\\Day2\\Input.txt");

    int min, max;
    char c, _;
    std::string password;
    size_t numberOfValidPasswords = 0;
    while (!(inputFile >> min >> _ >> max >> c >> _ >> password).eof())
    {
        //std::cout << min << "-" << max << " " << c << ": " << password << " -> " << (isValid(min, max, c, password) ? "valid" : "invalid") << std::endl;
        numberOfValidPasswords += isValid2(min, max, c, password) ? 1 : 0;
    }
    std::cout << numberOfValidPasswords;
}