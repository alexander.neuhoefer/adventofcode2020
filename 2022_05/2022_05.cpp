#include <iostream>
#include <fstream>
#include <map>
#include <deque>
#include <string>

static void display(const std::map<size_t, std::deque<char>>& stacks)
{
    std::cout << "--------" << std::endl;
    for(size_t i = 0; i < stacks.size(); ++i)
    {
        std::cout << i << ": ";
        for(const auto& e : stacks.at(i))
        {
            std::cout << e;
        }
        std::cout << std::endl;
    }
    std::cout << "--------" << std::endl;
}

using stack = std::deque<char>;

static void move9000(size_t count, stack& from, stack& to)
{
    for(size_t i = 0; i < count; ++i)
    {
        to.push_back(from.back());
        from.pop_back();
    }
}

static void move9001(size_t count, stack& from, stack& to)
{
    stack tmp;
    move9000(count, from, tmp);
    move9000(count, tmp, to);
}

int main()
{
    auto file = std::ifstream("Input.txt");
    std::string line;

    std::map<size_t, stack> stacks;
    while(std::getline(file, line) && line.at(1) != '1')
    {
        std::cout << line << std::endl;
        const size_t n = (line.size() + 1) / 4;
        for(size_t i = 0; i < n; ++i)
        {
            //1, 5, 9
            const auto c = line.at(i * 4 + 1);
            if(c != ' ')
            {
                stacks[i].push_front(c);
            }
        }
    }

    display(stacks);
    std::getline(file, line);

    size_t count, from, to;
    while(file >> line >> count >> line >> from >> line >> to)
    {
        std::cout << count << ": " << from << "->" << to << std::endl;
        move9001(count, stacks.at(from - 1), stacks.at(to - 1));
        //display(stacks);
    }

    std::cout << "final top elements: ";
    for(size_t i = 0; i < stacks.size(); ++i)
    {
        std::cout << stacks.at(i).back();
    }
}