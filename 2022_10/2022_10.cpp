#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

int main()
{
    auto file = std::ifstream("Input.txt");

    std::vector<int> history{1};
    int x = 1;
    std::string command;
    while(file >> command)
    {
        std::cout << "Executing " << command << " -> ";
        if(command == "noop")
        {
            history.push_back(x);
        }
        else if(command == "addx")
        {
            history.push_back(x);
            history.push_back(x);
            int argument;
            file >> argument;
            x += argument;
        }
        std::cout << "Cycle: " << history.size() << ", x is: " << x << std::endl;
    }

    int sumOfSignalStrengths = 0;
    for(int i = 20; i < history.size(); i += 40)
    {
        auto signalStrength = i * history.at(i);
        std::cout << "Signal strength at cycle " << i << " is:" << signalStrength << std::endl;
        sumOfSignalStrengths += signalStrength;
    }
    std::cout << "Sum of signal strengths: " << sumOfSignalStrengths << std::endl;

    for(int cycle = 1, y = 0; y < 6; ++y)
    {
        for(int x = 0; x < 40; ++x, ++cycle)
        {
            std::cout << (std::abs(x - history.at(cycle)) <= 1 ? '#' : '.');
        }
        std::cout << std::endl;
    }
}