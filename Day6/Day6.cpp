#include <iostream>
#include <fstream>
#include <map>
#include <array>
#include <string>
#include <regex>

void parseLine(std::array<uint16_t, 26>& trueAnswers, const std::string& line)
{
    for (const auto& c : line)
    {
        trueAnswers[c - 'a'] += 1;
    }
}

int main()
{
    auto inputFile = std::ifstream("Input.txt");

    std::string line;
    size_t sumOfTrueAnswers = 0;
    std::array<uint16_t, 26> trueAnswers = {0};
    uint16_t groupSize = 0;
    while (std::getline(inputFile, line))
    {
        if (line.empty())
        {
            std::cout << line << "_______ -> ";
            for (const auto& b : trueAnswers)
            {
                std::cout << b ? 1 : 0;
            }
            std::cout << std::endl;
            sumOfTrueAnswers += std::count(trueAnswers.cbegin(), trueAnswers.cend(), groupSize);
            trueAnswers = {0};
            groupSize = 0;
        }
        else
        {
            ++groupSize;
            std::cout << line << " -> ";
            parseLine(trueAnswers, line);
            for (const auto& b : trueAnswers)
            {
                std::cout << b ? 1 : 0;
            }
            std::cout << std::endl;
        }
    }
    sumOfTrueAnswers += std::count(trueAnswers.cbegin(), trueAnswers.cend(), groupSize);
    std::cout << sumOfTrueAnswers << std::endl;
    std::cin >> line;
}