﻿var stringToDigitMap = Enumerable.Range(0, 10)
    .ToDictionary(i => i.ToString());

#region part 2
int idx = 0;
foreach (var key in new[] { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" })
{
    stringToDigitMap.Add(key, ++idx);
}
#endregion

int? ToDigit(string s)
{
    foreach (var (d, v) in stringToDigitMap)
    {
        if (s.StartsWith(d))
        {
            return v;
        }
    }
    return null;
}

IEnumerable<int> ExtractDigits(string s)
{
    for (int i = 0; i < s.Length; ++i)
    {
        int? digit = ToDigit(s.Substring(i));
        if (digit.HasValue)
        {
            yield return digit.Value;
        }
    }
}

int sum = 0;
foreach (var line in File.ReadLines("./longInput.txt"))
{
    var digits = ExtractDigits(line).ToList();
    Console.WriteLine($"{line} => {digits.First()}{digits.Last()}");
    sum += digits.First() * 10 + digits.Last();
    Console.WriteLine("running sum is: " + sum);
}