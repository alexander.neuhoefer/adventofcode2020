#include <iostream>
#include <fstream>
#include <regex>

int main()
{
    auto file = std::ifstream("input.txt");
    auto mulRegex = std::regex("(mul\\((\\d{1,3}),(\\d{1,3})\\))|(do\\(\\))|(don't\\(\\))");

    int sum = 0;
    std::string line;
    auto enabled = true;
    while (std::getline(file, line))
    {
        auto begin = std::sregex_iterator(line.begin(), line.end(), mulRegex);
        auto end = std::sregex_iterator();
        for (std::sregex_iterator it = begin; it != end; ++it)
        {
            std::smatch match = *it;
            if (match[0] == "do()")
            {
                enabled = true;
                std::cout << match[0] << "=>enabled" << std::endl;
            }
            else if (match[0] == "don't()")
            {
                enabled = false;
                std::cout << match[0] << "=>disabled" << std::endl;
            } else {
                const auto a = std::stoi(match[2]);
                const auto b = std::stoi(match[3]);
                const auto product = a * b;
                if (enabled)
                {
                    sum += product;
                }
                std::cout << "found: '" << match[0] << "'" << "\t" << a << "x" << b << "=" << product << "\t" << sum << std::endl;
            }
        }
    }
}