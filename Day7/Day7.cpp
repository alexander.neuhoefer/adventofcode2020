#include <iostream>
#include <fstream>
#include <map>
#include <set>
#include <vector>
#include <string>
#include <regex>

static const std::regex lineRegex("^(.+) bags? contain ((\\d+) ([a-z ]+) bags?[, .]*)+");

void parseLine(std::map<std::string, std::set<std::string>>& parents, const std::string& line)
{
    std::smatch matches;
    if (std::regex_match(line, matches, lineRegex))
    {
        const auto container = matches[1].str();
        for(size_t i = 0; i < matches.size(); ++i)
        std::cout << matches[i].str() << std::endl;
        for (size_t i = 3; i < matches.size(); i += 3)
        {
            const auto count = matches[i].str();
            const auto containee = matches[i + 1].str();
            parents[containee].insert(container);
            std::cout << container << " -> " << containee << std::endl;
        }
    }
}

int main()
{
    auto inputFile = std::ifstream("Input.txt");

    std::string line;
    std::map<std::string, std::set<std::string>> parents;
    while (std::getline(inputFile, line))
    {
        std::cout << "_" << line << std::endl;
        parseLine(parents, line);
    }
}