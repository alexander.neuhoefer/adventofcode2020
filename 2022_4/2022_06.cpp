#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <ranges>

int main()
{
    auto file = std::ifstream("Input.txt");
    std::string input;
    file >> input;

    constexpr size_t MARKER_SIZE = 14;
    for(size_t i = MARKER_SIZE; i < input.size(); ++i)
    {
        auto window = input.substr(i - MARKER_SIZE, MARKER_SIZE);
        std::sort(window.begin(), window.end());
        auto end = std::unique(window.begin(), window.end());
        if(end == window.end())
        {
            std::cout << window << " at " << i << std::endl;
            break;
        }
    }
}