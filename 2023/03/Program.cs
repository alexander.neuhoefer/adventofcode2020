﻿var digits = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
var IsDigit = (char c) => digits.Contains(c);
var IsSymbol = (char c) => c != '.' && !IsDigit(c);

var InRange = (string line, int idx)
    => idx >= 0 && idx < line.Length;

var ContainsSymbolAt = (string line, int idx)
    => InRange(line, idx) && IsSymbol(line[idx]);

var lines = File.ReadAllLines("./longInput.txt").ToList();
var emptyLine = new string('.', lines.First().Length);
lines.Insert(0, emptyLine);
lines.Add(emptyLine);

int sumOfParts = 0, sumOfGearRatio = 0;
for(int y = 1; y < lines.Count - 1; ++y)
{
    var (prev, line, next) = (lines[y - 1], lines[y], lines[y + 1]);

    string currentDigit = "";
    for(int x = 0; x < line.Length; ++x)
    {
        sumOfGearRatio += GearRatio(prev, line, next, x);

        var c = line[x];
        if(IsDigit(c))
        {
            currentDigit += c;
            if (x < line.Length - 1)
            {
                continue;
            }
        }

        var length = currentDigit.Length;
        if (length > 0)
        {
            var valid = HasNeighboringSymbol(prev, line, next, x - length, x);
            sumOfParts += valid ? int.Parse(currentDigit) : 0;
        }
        currentDigit = "";
    }
}

Console.WriteLine($"Sum of engine parts: {sumOfParts}");
Console.WriteLine($"Sum of gear ratios: {sumOfGearRatio}");

bool HasNeighboringSymbol(
    string prevLine, string line, string nextLine,
    int xStart, int exclusiveXEnd)
{
    bool hasNeighboringSymbol = false;
    for(int x = xStart - 1; x <= exclusiveXEnd; ++x)
    {
        hasNeighboringSymbol |=
            ContainsSymbolAt(prevLine, x) ||
            ContainsSymbolAt(line, x) ||
            ContainsSymbolAt(nextLine, x);
    }
    return hasNeighboringSymbol;
}

bool IsGear(string prevLine, string line, string nextLine, int x)
{
    return line[x] == '*' && HasExactlyTwoNeighboringNumbers(prevLine, line, nextLine, x);
}

bool HasExactlyTwoNeighboringNumbers(string prevLine, string line, string nextLine, int x)
{
    var NumbersInLine = (string s, int x) => string.IsNullOrEmpty(s) ? 0
        : (IsDigit(s[x]) ? 1
            : (((InRange(s, x - 1) && IsDigit(s[x - 1])) ? 1 : 0)
            + ((InRange(s, x + 1) && IsDigit(s[x + 1])) ? 1 : 0)));

    int countOfNeighboringNumbers = NumbersInLine(prevLine, x) + NumbersInLine(line, x) + NumbersInLine(nextLine, x);
    return countOfNeighboringNumbers == 2;
}

int GearRatio(string prevLine, string line, string nextLine, int x)
{
    if(!IsGear(prevLine, line, nextLine, x))
    {
        return 0;
    }

    return ExtractNumbers(prevLine, x)
        .Concat(ExtractNumbers(line, x))
        .Concat(ExtractNumbers(nextLine, x))
        .Aggregate(1, (product, number) => product * number);
}

int SearchLeft(string line, int idx)
{
    while (InRange(line, idx) && IsDigit(line[idx]))
    {
        --idx;
    }
    return idx + 1;
}

int SearchRight(string line, int idx)
{
    while (InRange(line, idx) && IsDigit(line[idx]))
    {
        ++idx;
    }
    return idx;
}

List<int> ExtractNumbers(string line, int x)
{
    if (IsDigit(line[x]))
    {
        int start = SearchLeft(line, x - 1);
        int end = SearchRight(line, x + 1);
        return new List<int>{ int.Parse(line.Substring(start, end - start)) };
    }
    var numbers = new List<int>();
    if (IsDigit(line[x - 1]))
    {
        int start = SearchLeft(line, x - 1);
        numbers.Add(int.Parse(line.Substring(start, x - start)));
    }
    if (IsDigit(line[x + 1]))
    {
        int start = x + 1;
        int end = SearchRight(line, x + 2);
        numbers.Add(int.Parse(line.Substring(start, end - start)));
    }
    return numbers;
}