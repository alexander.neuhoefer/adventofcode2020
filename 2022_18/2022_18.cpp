#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <unordered_set>
#include <algorithm>

struct Point3D
{
    int32_t x,y,z;
    auto operator<=>(const Point3D& other) const = default;
};

template<>
struct std::hash<Point3D>
{
    size_t operator()(const Point3D& p) const noexcept
    {
        int32_t hash = p.x;
        hash ^= p.y << 8;
        hash ^= p.z << 16;
        return static_cast<size_t>(hash);
    }
};

std::istream& operator>>(std::istream& stream, Point3D& p)
{
    static char _;
    return stream >> p.x >> _ >> p.y >> _ >> p.z;
}

std::ostream& operator<<(std::ostream& stream, const Point3D& p)
{
    return stream << p.x << "," << p.y << "," << p.z;
}

std::unordered_set<Point3D> neighbors(const Point3D& p)
{
    return {
        {p.x - 1, p.y, p.z},
        {p.x + 1, p.y, p.z},
        {p.x, p.y - 1, p.z},
        {p.x, p.y + 1, p.z},
        {p.x, p.y, p.z - 1},
        {p.x, p.y, p.z + 1}
    };
}

int numberOfNeighborsIn(const std::unordered_set<Point3D>& cubes, const Point3D& p)
{
    int count = 0;
    for(const auto& n : neighbors(p))
    {
        count += cubes.contains(n);
    }
    return count;
}

int main()
{
    auto file = std::ifstream("Input.txt");

    Point3D cube, min = {100, 100, 10}, max = {0, 0, 0};
    size_t numberOfSides = 0;
    std::unordered_set<Point3D> cubes;
    while(file >> cube)
    {
        numberOfSides += 6 - numberOfNeighborsIn(cubes, cube) * 2;
        cubes.insert(cube);
        min = {std::min(min.x, cube.x), std::min(min.y, cube.y), std::min(min.z, cube.z)};
        max = {std::max(max.x, cube.x), std::max(max.y, cube.y), std::max(max.z, cube.z)};
        std::cout << cube << std::endl;
    }
    std::cout << "number of sides: " << numberOfSides << std::endl;

    std::unordered_set<Point3D> insideCandidates;
    for(int z = min.z; z <= max.z; ++z)
    {
        for(int y = min.y; y <= max.y; ++y)
        {
            for(int x = min.x; x <= max.x; ++x)
            {
                Point3D p = {x, y , z};
                if(!cubes.contains(p))
                {
                    insideCandidates.insert(p);
                }
            }
        }
    }

    std::unordered_set<Point3D> outsideCubes;
    for(int z = min.z; z <= max.z; ++z)
    {
        for(int y = min.y; y <= max.y; ++y)
        {
            outsideCubes.insert({min.x - 1, y, z});
            outsideCubes.insert({max.x + 1, y, z});
        }
    }
    for(int z = min.z; z <= max.z; ++z)
    {
        for(int x = min.x; x <= max.x; ++x)
        {
            outsideCubes.insert({x, min.y - 1, z});
            outsideCubes.insert({x, max.y + 1, z});
        }
    }
    for(int y = min.y; y <= max.y; ++y)
    {
        for(int x = min.x; x <= max.x; ++x)
        {
            outsideCubes.insert({x, y, min.z - 1});
            outsideCubes.insert({x, y, max.z + 1});
        }
    }

    size_t previousSize;
    do
    {
        previousSize = insideCandidates.size();
        auto copy = insideCandidates;
        for(const auto& p : insideCandidates)
        {
            const auto hood = neighbors(p);
            bool hasOutsideNeighbor = std::any_of(hood.cbegin(), hood.cend(), [&](const Point3D& n){
                return outsideCubes.contains(n);
            });
            if(outsideCubes.contains(p) || hasOutsideNeighbor)
            {
                copy.erase(p);
                outsideCubes.insert(p);
                for(const auto& n : hood)
                {
                    copy.erase(n);
                    if(!cubes.contains(n))
                    {
                        outsideCubes.insert(n);
                    }
                }
            }
        }
        insideCandidates = std::move(copy);
    }while(previousSize != insideCandidates.size());

    auto numberOfExteriorSides = numberOfSides;
    for(const auto& p : insideCandidates)
    {
        numberOfExteriorSides += 6 - numberOfNeighborsIn(cubes, p) * 2;
        cubes.insert(p);
    }
    std::cout << "number of exterior sides: " << numberOfExteriorSides << std::endl;
}