﻿using System.Numerics;

var grid = File.ReadAllLines("./longInput.txt").ToArray();
var width = grid.First().Length;
var height = grid.Length;

var galaxies = new List<Galaxy>();
for (int y = 0; y < height; ++y)
{
    for (int x = 0; x < width; ++x)
    {
        if (grid[y][x] == '#')
        {
            galaxies.Add(new Galaxy(x, y));
        }
    }
}

var expansionPointsAlongY = new List<int>();
for (int y = 0; y < height; ++y)
{
    if (grid[y].All(c => c == '.'))
        expansionPointsAlongY.Add(y);
}

var expansionPointsAlongX = new List<int>();
for (int x = 0; x < width; ++x)
{
    if(grid.All(s => s[x] == '.'))
        expansionPointsAlongX.Add(x);
}

var pairOfGalaxies = galaxies.SelectMany(
    (a, i) => galaxies.Skip(i + 1).Select(b => (a, b))
).ToList();

var InRange = (int x, int a, int b) =>
{
    var min = Math.Min(a, b);
    var max = Math.Max(a, b);
    return min <= x && x < max;
};

var CalcDistance = (Galaxy a, Galaxy b) =>
{
    BigInteger distance = expansionPointsAlongX.Count(x => InRange(x, a.X, b.X))
                        + expansionPointsAlongY.Count(y => InRange(y, a.Y, b.Y));
    distance *= 1000000 - 1;
    return distance + (Math.Abs(b.Y - a.Y) + Math.Abs(b.X - a.X));
};
var distances = pairOfGalaxies.Select(p => CalcDistance(p.Item1, p.Item2));

Console.WriteLine($"{string.Join('\n', grid)}");
Console.WriteLine($"Galaxies: {string.Join(',', galaxies)}");
Console.WriteLine($"Number of pairs: {pairOfGalaxies.Count()}");
Console.WriteLine($"Distances: {string.Join(',', distances)}");
Console.WriteLine($"Sum of distances: {distances.Aggregate((a,b) => a + b)}");

record Galaxy(int X, int Y);
