﻿var ToIntegerList = (string s) => s.Split(' ')
    .Select(i => int.Parse(i))
    .ToList();

var input = File.ReadAllLines("./longInput.txt")
    .Select(ToIntegerList)
    .ToList();

var sum = 0;
foreach(var sequence in input)
{
    Console.WriteLine($"i{string.Join("     ", sequence.Select(s => $"{s,5}"))}");
    var newValue = Extrapolate(sequence);
    sum += newValue;
}
Console.WriteLine($"Sum is {sum}");

int Extrapolate(List<int> sequence)
{
    var differences = new List<List<int>> { sequence };
    do
    {
        var diff = differences.Last();
        var d = diff.Skip(1).Zip(diff, (a, b) => a - b).ToList();
        differences.Add(d);
    } while (differences.Last().Any(i => i != 0));

    differences.Last().Insert(0, 0);
    for(int i = differences.Count - 2; i >= 0; --i)
    {
        var diff = differences[i];
        diff.Insert(0, diff.First() - differences[i + 1].First());
    }

    foreach(var d in differences)
    {
        Console.WriteLine($"\te{string.Join("     ", d.Select(s => $"{s,5}"))}");
    }
    return differences.First().First();
}