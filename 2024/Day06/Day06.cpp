#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <sstream>
#include <algorithm>

using Grid = std::vector<std::string>;

static std::ostream& operator<<(std::ostream& stream, const Grid& grid)
{
    for (const auto& line : grid)
    {
        stream << line << std::endl;
    }
    return stream;
}

struct Guard
{
    static Guard create(const Grid& grid)
    {
        const int height = static_cast<int>(grid.size());
        const int width = static_cast<int>(grid.front().size());
        for (int y = 0; y < height; ++y)
        {
            for (int x = 0; x < width; ++x)
            {
                if (grid[y][x] != '#' && grid[y][x] != '.')
                {
                    std::cout << "guard starts at: " << x << "/" << y << std::endl;
                    return { x, y, width, height };
                }
            }
        }
        std::cout << "could not find guard" << std::endl;
        return {};
    }

    static size_t countDistinctPositions(const Grid& grid)
    {
        size_t count = 0;
        for (const auto& line : grid)
        {
            count += std::count(line.cbegin(), line.cend(), 'X');
        }
        return count;
    }

    bool moveOrTurn(Grid& grid)
    {
        switch (grid[y][x])
        {
        case '^':
            if (grid[y - 1][x] != '#')
            {
                grid[y][x] = 'X';
                grid[--y][x] = '^';
            } else grid[y][x] = '>';
            break;
        case '>':
            if (grid[y][x + 1] != '#')
            {
                grid[y][x] = 'X';
                grid[y][++x] = '>';
            } else grid[y][x] = 'v';
            break;
        case 'v':
            if (grid[y + 1][x] != '#')
            {
                grid[y][x] = 'X';
                grid[++y][x] = 'v';
            } else grid[y][x] = '<';
            break;
        case '<':
            if (grid[y][x - 1] != '#')
            {
                grid[y][x] = 'X';
                grid[y][--x] = '<';
            } else grid[y][x] = '^';
            break;
        }
        return x < 0 || x >= width
            || y < 0 || y >= height;
    }

    int x = 0, y = 0;
    int width = 0, height = 0;
};

int main()
{
    auto file = std::ifstream("input.txt");

    std::string line;
    Grid grid;
    while (std::getline(file, line))
    {
        grid.push_back(line);
    }

    auto guard = Guard::create(grid);
    do
    {
        //std::cout << grid << std::endl;
    } while (!guard.moveOrTurn(grid));
    std::cout << grid << std::endl;

    std::cout << "Distinct spaces visited: " << Guard::countDistinctPositions(grid) << std::endl;
}