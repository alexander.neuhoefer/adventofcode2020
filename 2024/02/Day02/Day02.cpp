#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <sstream>
#include <functional>

static std::vector<int> stringToListOfNumbers(const std::string& line)
{
    std::vector<int> list;
    int n;
    auto stream = std::stringstream(line);
    while (stream.good())
    {
        stream >> n;
        list.push_back(n);
    }
    return list;
}

static std::ostream& operator<<(std::ostream& stream, const std::vector<int>& numbers)
{
    for (const auto& n : numbers)
    {
        stream << n << ' ';
    }
    return stream;
}

static bool wrongDistance(int a, int b)
{
    auto distance = std::abs(a - b);
    return distance < 1 || distance > 3;
}

static bool isSafe(const std::vector<int>& numbers)
{
    return std::adjacent_find(numbers.cbegin(), numbers.cend(), wrongDistance) == numbers.cend() &&
        (  std::is_sorted(numbers.cbegin(), numbers.cend(), std::less<int>())
        || std::is_sorted(numbers.cbegin(), numbers.cend(), std::greater<int>()));
}

static bool isSafeWithDampener(const std::vector<int> numbers)
{
    for (int i = 0; i < numbers.size(); ++i)
    {
        auto copy = numbers;
        copy.erase(copy.begin() + i);
        if (isSafe(copy))
        {
            return true;
        }
    }
    return false;
}

int main()
{
    auto file = std::ifstream("input.txt");

    int numberOfSafeReports = 0;
    int numberOfSafeReportsWithDampener = 0;
    std::string line;
    while (std::getline(file, line))
    {
        const auto numbers = stringToListOfNumbers(line);
        numberOfSafeReports += static_cast<int>(isSafe(numbers));
        numberOfSafeReportsWithDampener += static_cast<int>(isSafeWithDampener(numbers));
        std::cout << numbers << "\t" << isSafe << "\t" << numberOfSafeReports << "\t" << numberOfSafeReportsWithDampener << std::endl;
    }

    std::cout << "NumberOfSafeReports: " << numberOfSafeReports << std::endl;
    std::cout << "NumberOfSafeReportsWithDampener: " << numberOfSafeReportsWithDampener << std::endl;
}