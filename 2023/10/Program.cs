﻿var input = File.ReadAllLines("./longInput.txt");

//Avoid checking indices by ensuring we have an empty border all around
var emptyLine = new string('.', input.First().Length);
input = input.Prepend(emptyLine).Append(emptyLine).Select(s => '.' + s + '.').ToArray();

var locations = input.Select(
    (s, y) => s.Select(
        (c, x) => new Location(c, x, y)).ToArray()
    ).ToArray();

var queque = locations.SelectMany(list => list.Where(l => l.Type == 'S')).ToList();
var start = queque.Single();
Console.WriteLine($"Starting point is {start}");

start.Cost = 0;
while(queque.Any())
{
    var current = queque.First();
    queque.RemoveAt(0);
    if (current.Processed)
        continue;

    current.Processed = true;
    var neighbors = GetConnectedNeighbors(current)
        .Where(l => l.Processed == false).ToList();
    if (!neighbors.Any())
        continue;

    foreach (var n in neighbors.Where(n => n.Cost < 0))
    {
        n.Cost = current.Cost + 1;
    }

    queque.AddRange(neighbors);
    //Console.WriteLine($"Processed {current}");
}

Console.WriteLine($"Max cost is {locations.SelectMany(_ => _).Max(l => l.Cost)}");

Visualize(locations, (Location l) => l.Type.ToString());
Visualize(locations, (Location l) => l.Cost < 0 ? "." : l.Cost.ToString());

UpdateInside(locations);
Visualize(locations, (Location l) => l.IsInside ? "X" : l.Cost < 0 ? "." : l.Type.ToString());
var numberOfInsideLocations = locations.Aggregate(0, (v, ll) => v + ll.Count(l => l.IsInside));
Console.WriteLine($"Number of inside lcoations: {numberOfInsideLocations}");

List <Location> GetConnectedNeighbors(Location current)
{
    return current.Type switch
    {
        '|' => new List<Location> { locations[current.Y - 1][current.X], locations[current.Y + 1][current.X] },
        '-' => new List<Location> { locations[current.Y][current.X - 1], locations[current.Y][current.X + 1] },
        'L' => new List<Location> { locations[current.Y - 1][current.X], locations[current.Y][current.X + 1] },
        'J' => new List<Location> { locations[current.Y - 1][current.X], locations[current.Y][current.X - 1] },
        '7' => new List<Location> { locations[current.Y + 1][current.X], locations[current.Y][current.X - 1] },
        'F' => new List<Location> { locations[current.Y + 1][current.X], locations[current.Y][current.X + 1] },
        'S' => new List<Location>{
            locations[current.Y - 1][current.X], locations[current.Y + 1][current.X],
            locations[current.Y][current.X - 1], locations[current.Y][current.X + 1]
        }.Where(n => GetConnectedNeighbors(n).Contains(current)).ToList(),
        _ => new List<Location> { },
    };
}

void UpdateInside(Location[][] locations)
{
    char[] borders = "|LJ".ToCharArray();
    foreach (var row in locations)
    {
        bool inside = false;
        foreach(var l in row)
        {
            if (l.Cost < 0)
                l.IsInside = inside;
            else if (borders.Contains(ActualType(l)))
                inside = !inside;
        }
    }

    char ActualType(Location loc)
    {
        if (loc.Type != 'S') return loc.Type;
        var neighbors = GetConnectedNeighbors(loc);
        if (neighbors.Count(n => n.Y == loc.Y) == 2) return '-';
        else if (neighbors.Count(n => n.X == loc.X) == 2) return '|';
        else if (neighbors.Count(n => n.Y == loc.Y - 1 || n.X == loc.X + 1) == 2) return 'L';
        else if (neighbors.Count(n => n.Y == loc.Y - 1 || n.X == loc.X - 1) == 2) return 'J';
        else if (neighbors.Count(n => n.Y == loc.Y + 1 || n.X == loc.X + 1) == 2) return 'F';
        else if (neighbors.Count(n => n.Y == loc.Y + 1 || n.X == loc.X - 1) == 2) return '7';
        else throw new Exception("Something went wrong here");
    };
}

void Visualize(Location[][] locations, Func<Location, string> f)
{
    foreach(var row in locations)
    {
        foreach(var l in row)
        {
            Console.Write(f(l));
        }
        Console.WriteLine();
    }
    Console.WriteLine();
}

record Location(char Type, int X, int Y)
{
    public int Cost { get; set; } = -1;
    public bool Processed { get; set; } = false;
    public bool IsInside { get; set; } = false;
}
