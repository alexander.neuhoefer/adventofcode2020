#include <iostream>

template<int Depth>
constexpr size_t sum(size_t x)
{
    return sum<Depth-1>(2 * x + 0)
        + sum<Depth - 1>(2 * x + 1);
}

template<>
constexpr size_t sum<0>(size_t x)
{
    return x;
}

int main()
{
    std::cout << sum<30>(0);
}