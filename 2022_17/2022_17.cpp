#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

struct Point
{
    int x, y;
};

struct Shape
{
    Shape(std::vector<std::string> data)
        : data(std::move(data))
    {}

    size_t width() const
    {
        return data.front().size();
    }

    size_t height() const
    {
        return data.size();
    }

    std::vector<std::string> data;
};

static std::vector<Shape> rockShapes = {
    Shape(std::vector<std::string>{
        {"####"}
    }),
    Shape(std::vector<std::string>{
        {".#."},
        {"###"},
        {".#."}
    }),
    Shape(std::vector<std::string>{
        {"###"},
        {"..#"},
        {"..#"}
    }),
    Shape(std::vector<std::string>{
        {"#"},
        {"#"},
        {"#"},
        {"#"}
    })
};

void print(const Shape& cavern)
{
    std::for_each(cavern.data.crbegin(), cavern.data.crend(), [](const std::string& row) {
        std::cout << "|" << row << "|" << std::endl;
    });
    std::cout << "+-------+" << std::endl;
}

bool overlaps(const Shape& a, const Shape& b, Point offset)
{
    for(int y = 0; y < b.height(); ++y)
    {
        for(int x = 0; x < b.width(); ++x)
        {
            int x2 = x + offset.x;
            int y2 = y + offset.y;
            if(b.data[y][x] == '#' && a.data[y2][x2] == '#')
            {
                return true;
            }
        }
    }
    return false;
}

void insert(Shape& a, const Shape& b, Point offset)
{
    for(int y = 0; y < b.height(); ++y)
    {
        for(int x = 0; x < b.width(); ++x)
        {
            int x2 = x + offset.x;
            int y2 = y + offset.y;
            if(b.data[y][x] == '#')
            {
                a.data[y2][x2] = '#';
            }
        }
    }
}

int main()
{
    auto file = std::ifstream("Input.txt");
    std::string jetPattern;
    std::getline(file, jetPattern);

    std::string emptyRow(".......");
    Shape cavern(std::vector<std::string>{3, emptyRow});

    constexpr int ROCKS_TO_DROP = 3;
    int previousRockTop = -1, jetIndex = 0;
    for(int i = 0; i < ROCKS_TO_DROP; ++i)
    {
        const auto& shape = rockShapes[i % rockShapes.size()];
        Point rockPosition = {2, previousRockTop + 4};

        while(cavern.height() <= rockPosition.y + shape.height())
        {
            cavern.data.push_back(emptyRow);
        }

        //print(cavern);

        bool push = true;
        while(true)
        {
            if(push)
            {
                push = false;
                auto jet = jetPattern[jetIndex++];
                if(jet == '>' && rockPosition.x + shape.width() < cavern.width())
                {
                    rockPosition.x += 1;
                    if(overlaps(cavern, shape, rockPosition))
                    {
                        rockPosition.x -= 1;
                    }
                }
                if(jet == '<' && rockPosition.x > 0)
                {
                    rockPosition.x -= 1;
                    if(overlaps(cavern, shape, rockPosition))
                    {
                        rockPosition.x += 1;
                    }
                }
            }
            else
            {
                push = true;
                if(rockPosition.y == 0)
                {
                    insert(cavern, shape, rockPosition);
                    break;
                }

                rockPosition.y -= 1;

                if(overlaps(cavern, shape, rockPosition))
                {
                    rockPosition.y += 1;
                    insert(cavern, shape, rockPosition);
                    break;
                }
            }
        }

        print(cavern);
    }
}