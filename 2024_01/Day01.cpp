#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>

int main()
{
    std::vector<int> listA, listB;
    int a, b;

    auto file = std::ifstream("input.txt");
    while (file.good())
    {
        file >> a >> b;
        listA.push_back(a);
        listB.push_back(b);
    }
    std::cout << a << '\t' << b << std::endl;

    std::sort(listA.begin(), listA.end());
    std::sort(listB.begin(), listB.end());

    size_t sumOfDistances = 0;
    for (size_t i = 0; i < listA.size(); ++i)
    {
        const auto& a = listA.at(i), b = listB.at(i);
        const auto distance = std::abs(a - b);
        sumOfDistances += distance;
        std::cout << a << '\t' << b << '\t' << distance << '\t' << sumOfDistances << std::endl;
    }

    //----------p2-----
    size_t similarityScore = 0;
    for (const auto& a : listA)
    {
        const auto occurences = std::count(listB.cbegin(), listB.cend(), a);
        similarityScore += a * occurences;
    }

    std::cout << "SumOfDistances: " << sumOfDistances << std::endl;
    std::cout << "SimilarityScore: " << similarityScore << std::endl;
}