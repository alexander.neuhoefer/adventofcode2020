﻿var input = File.ReadAllLines("./shortInput.txt");

var width = input.First().Length;
var height = input.Count();
var grid = input.SelectMany(s => s.ToCharArray())
    .Select(c => int.Parse("" + c))
    .ToArray();

void Visualize(int[] g)
{
    foreach(var row in grid.Chunk(width))
    {
        Console.WriteLine($"{string.Join(null, row)}");
    }
}

Visualize(grid);
int Distance(int a, int b) => Math.Abs(a - b);
bool OnGrid(int idx) => 0 <= idx && idx < grid!.Length;

int CalculateCost(List<int> path, int j)
{
    var distance = Distance(path.Last(), j);
    var lastFewLocations = path.Append(j).Reverse().Take(5).ToArray();
    if (Reverses() || TooStraight() || !OnGrid(j) || !AreNeighbors())
    {
        return 10000;
    }
    return grid[j];

    bool TooStraight() => lastFewLocations.Count() == 5
        && (lastFewLocations.Skip(1).Zip(lastFewLocations).All(p => Math.Abs(p.Second - p.First) == 1)
        || lastFewLocations.Skip(1).Zip(lastFewLocations).All(p => Math.Abs(p.Second - p.First) == width));
    bool Reverses() => path.Count() > 2 && j == path.Reverse<int>().Skip(1).First();
    bool AreNeighbors() => distance == 1 || distance == width;
}

int[] GetNeighbors(int i) => [
    (i % width == 0) ? -1 : (i-1),
    ((i+1) % width == 0) ? -1 : (i+1),
    i - width,
    i + width];

var goal = grid.Length - 1;
var paths = new Dictionary<int, Path>
{
    { 0, new Path([0], 0) }
};

bool UpdateCosts(int i, Path path)
{
    if (!paths.ContainsKey(i))
    {
        paths.Add(i, path);
        return true;
    }

    if(path.Cost < paths[i].Cost)
    {
        paths[i] = path;
        return true;
    }
    return false;
}

Path Append(Path p, int idx) => new Path(
    p.Steps.Append(idx).ToList(),
    p.Cost + CalculateCost(p.Steps, idx)
);

bool updated = false;
do
{
    updated = false;
    for(int i = 0; i < grid.Length; ++i)
    {
        foreach (var n in GetNeighbors(i).Where(OnGrid))
        {
            if (paths.ContainsKey(i))
            {
                var path = Append(paths[i], n);
                updated |= UpdateCosts(n, path);
            }
        }
    }
    Console.WriteLine($"total costs: {paths.Sum(kvp => kvp.Value.Cost)} for {paths.Keys.Count()} entries");
} while (updated);

Console.WriteLine($"Getting to goal costs: {paths[goal].Cost} via {string.Join(",", paths[goal].Steps)}");
Console.WriteLine($"Checking costs: {paths[goal].Steps.Skip(1).Sum(i => grid[i])}");
List<int> exampleSteps = [0, 1, 2, 15, 16, 17, 18, 5, 6, 7, 8, 21, 34, 35, 36, 49, 62, 63, 76, 89, 102, 103, 116, 129, 142, 141, 154, 167, 168];
Console.WriteLine($"Checking example costs: { exampleSteps.Skip(1).Sum(i => grid[i])}");
record Path(List<int> Steps, int Cost);
