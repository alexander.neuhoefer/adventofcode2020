#include <iostream>
#include <fstream>
#include <functional>
#include <string>
#include <map>

int main()
{
    auto inputFile = std::ifstream("Input.txt");

    int position = 0, depth = 0, aim = 0;
    const std::map<std::string, std::function<void(int)>> commands =
    {
        {"down", [&](int x) { aim += x; } },
        {"up", [&](int x) { aim -= x; } },
        {"forward", [&](int x) { position += x, depth += aim * x; } },
    };

    std::string command; int amount;
    while (!(inputFile >> command >> amount).eof())
    {
        commands.at(command)(amount);
    }

    std::cout << position << "; " << depth << std::endl;
    std::cout << "result: " << position * depth << std::endl;
}