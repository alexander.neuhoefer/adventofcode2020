﻿var input = File.ReadAllLines("./longInput.txt").ToList();

var seeds = input.First().Split(' ').Skip(1).Select(Range.AsNumber).ToList();
Console.WriteLine(string.Join(',', seeds));

var FullRange = new Range(0, 0, long.MaxValue);
var ExtractMap = (List<string> list, string name) =>
    list.SkipWhile(s => !s.StartsWith(name))
    .Skip(1).TakeWhile(s => !string.IsNullOrWhiteSpace(s))
    .Select(Range.AsRange)
    .Append(FullRange)
    .ToList();

var seedToSoilMap = ExtractMap(input, "seed-to-soil");
var soilToFertilizer = ExtractMap(input, "soil-to-fertilizer");
var fertilizerToWater = ExtractMap(input, "fertilizer-to-water");
var waterToLight = ExtractMap(input, "water-to-light");
var lightToTemperature = ExtractMap(input, "light-to-temperature");
var temperatureToHumidity = ExtractMap(input, "temperature-to-humidity");
var humidityToLocation = ExtractMap(input, "humidity-to-location");

var Map = (List<Range> ranges, long i) =>
    ranges.Select(r => r.Map(i))
    .First(o => o.HasValue)
    .Value;

var SeedToLocation = (long seed) =>
{
    var soil = Map(seedToSoilMap, seed);
    var fertilizer = Map(soilToFertilizer, soil);
    var water = Map(fertilizerToWater, fertilizer);
    var light = Map(waterToLight, water);
    var temperature = Map(lightToTemperature, light);
    var humidity = Map(temperatureToHumidity, temperature);
    var location = Map(humidityToLocation, humidity);
    //Console.WriteLine($"{seed}->{soil}->{fertilizer}->{water}->{light}->{temperature}->{humidity}->{location}");
    return location;
};

var SeedRangeToMinLocation = (long start, long end) =>
{
    long minLocation = long.MaxValue;
    for (long seed = start; seed < end; ++seed)
    {
        var location = SeedToLocation(seed);
        if (location < minLocation)
        {
            minLocation = location;
            Console.WriteLine(minLocation);
        }
    }
    return minLocation;
};

var lowestLocation = seeds.Min(SeedToLocation);
var lowestLocation2 = seeds.Zip(seeds.Skip(1))
    .Where((a, index) => int.IsEvenInteger(index))
    .AsParallel()
    .Select(a => SeedRangeToMinLocation(a.First, a.First + a.Second))
    .Min();

Console.WriteLine($"Lowest location is {lowestLocation}");
Console.WriteLine($"Lowest location2 is {lowestLocation2}");

record Range(long DstStart, long SrcStart, long Length)
{
    public static long AsNumber(string s) => long.Parse(s);

    public static Range AsRange(string s)
    {
        var parts = s.Split(' ').Select(AsNumber).ToArray();
        return new Range(parts[0], parts[1], parts[2]);
    }

    public long? Map(long i)
    {
        var offset = i - SrcStart;
        if (0 <= offset && offset < Length)
            return DstStart + offset;

        return null;
    }
}