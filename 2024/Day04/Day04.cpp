#include <iostream>
#include <fstream>
#include <regex>
#include <sstream>

int matches(const std::string& word)
{
    return static_cast<int>(word == "XMAS" || word == "SAMX");
}

int matchesMas(const std::string& word)
{
    return static_cast<int>(word == "MAS" || word == "SAM");
}

int horizontalMatches(const std::vector<std::string>& fullText, int x, int y, int width, int height)
{
    if (width - x < 4) return 0;
    return matches({ fullText[y][x], fullText[y][x + 1], fullText[y][x + 2], fullText[y][x + 3] });
}

int verticalMatches(const std::vector<std::string>& fullText, int x, int y, int width, int height)
{
    if (height - y < 4) return 0;
    return matches({ fullText[y][x], fullText[y + 1][x], fullText[y + 2][x], fullText[y + 3][x] });
}

int forwardDiagonalMatches(const std::vector<std::string>& fullText, int x, int y, int width, int height)
{
    if (width - x < 4 || height - y < 4) return 0;
    return matches({ fullText[y][x], fullText[y + 1][x + 1], fullText[y + 2][x + 2], fullText[y + 3][x + 3] });
}

int backwardDiagonalMatches(const std::vector<std::string>& fullText, int x, int y, int width, int height)
{
    if (x < 3 || height - y < 4) return 0;
    return matches({ fullText[y][x], fullText[y + 1][x - 1], fullText[y + 2][x - 2], fullText[y + 3][x - 3] });
}

int crossMatches(const std::vector<std::string>& fullText, int x, int y, int width, int height)
{
    if (x < 1 || height < 1 || width - x < 2 || height - y < 2) return 0;
    return matchesMas({ fullText[y - 1][x - 1], fullText[y][x], fullText[y + 1][x + 1] })
        * matchesMas({ fullText[y - 1][x + 1], fullText[y][x], fullText[y + 1][x - 1] });
}

int main()
{
    auto file = std::ifstream("input.txt");

    std::string line;
    std::vector<std::string> fullText;
    while (std::getline(file, line))
    {
        fullText.push_back(line);
        std::cout << line << std::endl;
    }

    int width = static_cast<int>(fullText.front().size());
    int height = static_cast<int>(fullText.size());
    int occurences = 0, xmasOccurences = 0;
    for (int y = 0; y < height; ++y)
    {
        for (int x = 0; x < width; ++x)
        {
            occurences += horizontalMatches(fullText, x, y, width, height);
            occurences += verticalMatches(fullText, x, y, width, height);
            occurences += forwardDiagonalMatches(fullText, x, y, width, height);
            occurences += backwardDiagonalMatches(fullText, x, y, width, height);
            xmasOccurences += crossMatches(fullText, x, y, width, height);
        }
    }

    std::cout << "Found occurences/xmas: " << occurences << "/" << xmasOccurences << std::endl;
}