#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <sstream>
#include <algorithm>

static void parseDependency(std::map<int, std::vector<int>>& reliesOn, const std::string& line)
{
    int a, b; char _;
    std::stringstream(line) >> a >> _ >> b;

    reliesOn[b].push_back(a);
}

static std::vector<int> stringToListOfNumbers(const std::string& line)
{
    std::vector<int> list;
    int n; char _;
    auto stream = std::stringstream(line);
    while (stream.good())
    {
        stream >> n >> _;
        list.push_back(n);
    }
    return list;
}

static std::ostream& operator<<(std::ostream& stream, const std::vector<int>& numbers)
{
    for (const auto& n : numbers)
    {
        stream << n << ' ';
    }
    return stream;
}

static bool checkOrder(const std::vector<int>& pageOrder, std::map<int, std::vector<int>> reliesOn)
{
    auto inUpdate = [&pageOrder](int a) {return std::find(pageOrder.cbegin(), pageOrder.cend(), a) != pageOrder.cend(); };
    for (auto& n : pageOrder)
    {
        const auto& dependencies = reliesOn[n];
        if (!dependencies.empty() && std::any_of(dependencies.cbegin(), dependencies.cend(), inUpdate))
        {
            std::cout << "warning: " << n << " requires " << reliesOn[n] << std::endl;
            return false;
        }
        for (auto& [_, required] : reliesOn)
        {
            auto it = std::remove(required.begin(), required.end(), n);
            if (it != required.end()) required.erase(it);
        }
    }
    return true;
}

static void fixOrder(std::vector<int>& pageOrder, std::map<int, std::vector<int>> reliesOn)
{
    auto inUpdate = [&pageOrder](int a) {return std::find(pageOrder.cbegin(), pageOrder.cend(), a) != pageOrder.cend(); };
    for (auto& [key, deps] : reliesOn)
    {
        auto it = std::remove_if(deps.begin(), deps.end(), [&inUpdate](int n) { return !inUpdate(n); });
        deps.erase(it, deps.end());
    }
    for (int i = 0; i < pageOrder.size(); ++i)
    {
        auto& n = pageOrder[i];
        const auto& dependencies = reliesOn[n];
        if (!dependencies.empty())
        {
            for (auto& d : dependencies)
            {
                auto dep = std::find(pageOrder.begin() + i, pageOrder.end(), d);
                if (dep != pageOrder.end())
                {
                    std::cout << "swapping " << n << " with " << *dep << std::endl;
                    std::swap(n, *dep);
                }
            }
        }
    }
}

int main()
{
    auto file = std::ifstream("input.txt");

    std::string line;
    std::map<int, std::vector<int>> reliesOn;
    int sum = 0;
    int sumCorrected = 0;
    while (std::getline(file, line))
    {
        if (line.contains('|'))
        {
            std::cout << line << std::endl;
            parseDependency(reliesOn, line);
        } else {
            auto pageOrder = stringToListOfNumbers(line);
            if (line.empty() || line == "" || pageOrder.empty()) continue;
            auto fine = checkOrder(pageOrder, reliesOn);
            auto middle = pageOrder.begin() + pageOrder.size() / 2;
            sum += fine ? *middle : 0;
            std::cout << line << "\tfine? " << (fine ? "y" : "n") << "\t" << *middle << "\t" << sum << std::endl;

            //p2
            if (!fine)
            {
                fixOrder(pageOrder, reliesOn);
                sumCorrected += *middle;
                std::cout << "\tcorrected: " << pageOrder << "\t" << *middle << "\t" << sumCorrected << std::endl;
            }
        }
    }
    std::cout << "sum correct/corrected: " << sum << "/" << sumCorrected << std::endl;
}