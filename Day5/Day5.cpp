#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <string>
#include <regex>

uint16_t parseLine(const std::string& line)
{
    uint16_t id = 0;
    for (const auto& c : line)
    {
        id = id << 1;
        id |= (c == 'B' || c == 'R') ? 1 : 0;
    }
    return id;
}

int main()
{
    auto inputFile = std::ifstream("Input.txt");

    std::string line;
    uint16_t maxSeatID = 0;
    std::vector<bool> seen(979, false);
    while (std::getline(inputFile, line))
    {
        std::cout << "_" << line << " -> ";
        uint16_t seatID = parseLine(line);
        seen[seatID] = true;
        maxSeatID = std::max(maxSeatID, seatID);
        std::cout << "row: " << (seatID >> 3) << ", col: " << seatID % 8 << std::endl;
    }
    std::cout << "unseen: ";
    for (size_t i = 15; i < seen.size() - 16; ++i)
    {
        if (!seen[i])
        {
            std::cout << i << ", ";
        }
    }
}