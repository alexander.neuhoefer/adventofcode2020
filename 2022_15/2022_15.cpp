#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <string>
#include <chrono>

using namespace std;

struct Point2D
{
    int x, y;

    auto operator<=>(const Point2D& other) const = default;

    int distance(const Point2D& other) const
    {
        return std::abs(x - other.x) + std::abs(y - other.y);
    }
};

struct Sensor
{
    Point2D position, nearestBeacon;

    int range() const
    {
        return position.distance(nearestBeacon);
    }

    bool covers(const Point2D& p) const
    {
        return position.distance(p) <= range();
    }

    int distance(const Point2D& p) const
    {
        return position.distance(p);
    }
};


static ostream& operator<<(ostream& stream, const Point2D& p)
{
    return stream << "(" << p.x << "/" << p.y << ")";
}

static ostream& operator<<(ostream& stream, const Sensor& s)
{
    return stream << "S at " << s.position << ", nearest beacon: " << s.nearestBeacon;
}

int consumeInteger(std::string& line)
{
    line = line.substr(line.find('=') + 1);
    int a;
    stringstream(line) >> a;
    return a;
}

Point2D consumePoint(std::string& line)
{
    int x = consumeInteger(line);
    int y = consumeInteger(line);
    return { x, y };
}

size_t tuningFrequency(const Point2D& p)
{
    return static_cast<size_t>(p.x) * 4000000UL + static_cast<size_t>(p.y);
}

int main()
{
    auto file = ifstream("Input.txt");

    vector<Sensor> sensors;
    string line;
    int minX = numeric_limits<int>::max();
    int maxX = numeric_limits<int>::min();
    while (getline(file, line))
    {
        auto position = consumePoint(line);
        auto beacon = consumePoint(line);
        Sensor sensor{ position, beacon };
        minX = min<int>(minX, position.x - sensor.range());
        maxX = max<int>(maxX, position.x + sensor.range());
        sensors.push_back(sensor);

        cout << sensor << endl;
    }

    int blocked = 0;
    int y = 2000000;
    for (int x = minX; x <= maxX; ++x)
    {
        Point2D p{ x, y };
        bool noBeacon = none_of(sensors.cbegin(), sensors.cend(), [=](const Sensor& s)
            {
                return s.nearestBeacon == p;
            })
            && any_of(sensors.cbegin(), sensors.cend(), [=](const Sensor& s)
            {
                auto isSensor = s.position == p;
                auto isInRange = s.covers(p);
                return isSensor || isInRange;
            });
        blocked += static_cast<int>(noBeacon);
    }
    cout << "Blocked positions: " << blocked << endl;

    Point2D minCorner{ numeric_limits<int>::max(), numeric_limits<int>::max() };
    Point2D maxCorner{ numeric_limits<int>::min(), numeric_limits<int>::min() };
    for (const auto& s : sensors)
    {
        minCorner.x = std::min(minCorner.x, s.position.x);
        minCorner.y = std::min(minCorner.y, s.position.y);

        maxCorner.x = std::max(maxCorner.x, s.position.x);
        maxCorner.y = std::max(maxCorner.y, s.position.y);
    }
    cout << minCorner << " -> " << maxCorner << endl;

    auto start = chrono::high_resolution_clock::now();
    for (int y = minCorner.y; y <= maxCorner.y; ++y)
    {
        if (y % 10000 == 0)
        {
            cout << "\r" << y;
        }
        for (int x = minCorner.x; x <= maxCorner.x; ++x)
        {
            Point2D p{ x, y };
            bool isCandidate = true;
            for (const auto& s : sensors)
            {
                if (s.covers(p))
                {
                    isCandidate = false;
                    x += s.range() - s.distance(p);
                    break;
                }
            }
            if (isCandidate)
            {
                std::cout << endl <<  "found: " << p << ", tuning frequency: " << tuningFrequency(p) << endl;
            }
        }
    }
    auto end = chrono::high_resolution_clock::now();
    std::cout << endl << "searching took: " << chrono::duration_cast<chrono::milliseconds>(end - start).count();
}
