#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <numeric>
#include <algorithm>
#include <ranges>
#include <map>

using Item = char;
using Rucksack = std::string;

int toPriority(const Item& item)
{
    if(item > 'Z')
    {
        return item - 'a' + 1;
    }
    return item - 'A' + 27;
}

int main()
{
    auto file = std::ifstream("Input.txt");

    Rucksack rucksack;
    std::vector<Rucksack> allRucksacks;
    int sumOfPriorities = 0;
    while (std::getline(file, rucksack))
    {
        allRucksacks.emplace_back(rucksack.begin(), rucksack.end());

        const auto halfSize = rucksack.size() / 2;
        auto firstCompartment = rucksack.substr(0, halfSize);
        auto secondCompartment = rucksack.substr(halfSize);

        std::ranges::sort(firstCompartment);
        std::ranges::sort(secondCompartment);

        Rucksack duplicates;
        std::ranges::set_intersection(firstCompartment, secondCompartment, std::back_inserter(duplicates));
        auto end = std::unique(duplicates.begin(), duplicates.end());

        auto delta = std::transform_reduce(duplicates.begin(), end, 0, std::plus<int>(), toPriority);
        sumOfPriorities += delta;
        std::cout << rucksack << " " << duplicates << " " << delta << std::endl;
    }

    int sumOfBadges = 0;
    for(size_t i = 0; i < allRucksacks.size(); i += 3)
    {
        std::map<Item, int> count;
        for(auto j = i; j < i + 3; ++j)
        {
            auto& items = allRucksacks[j];
            std::ranges::sort(items);
            auto end = std::unique(items.begin(), items.end());
            std::for_each(items.begin(), end, [&](Item item)
            {
                count[item] += 1;
            });
        }
        for(const auto [key, value] : count)
        {
            if(value == 3)
            {
                const auto delta = toPriority(key);
                sumOfBadges += delta;
                std::cout << key << " " << delta << std::endl;
                break;
            }
        }
    }

    std::cout << "sumOfPriorities: " << sumOfPriorities << std::endl;
    std::cout << "sumOfBadges: " << sumOfBadges << std::endl;
}