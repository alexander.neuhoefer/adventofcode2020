﻿var bricks = File.ReadAllLines("./shortInput.txt")
    .Select(line => new Brick(line))
    .ToList();

Console.WriteLine($"{string.Join('\n', bricks)}");

var orderedBricks = bricks.OrderBy(b => b.Min(2)).ToList();

Console.WriteLine($"Order (ID,MinZ): {string.Join(',', orderedBricks.Select(b => (b.ID, b.Min(2))))}");

foreach (var brick in orderedBricks)
{
    var potentiallySupportingBricks = orderedBricks.Where(b => b.Max(2) < brick.Min(2)).ToList();
    brick.Fall(potentiallySupportingBricks);
}

Console.WriteLine("After falling:");
Console.WriteLine($"{string.Join('\n', orderedBricks)}");

Dictionary<int, List<int>> supportedBy = orderedBricks.ToDictionary(b => b.ID, _ => new List<int>());
foreach (var brick in orderedBricks)
{
    foreach(var other in orderedBricks)
    {
        if(brick.ID != other.ID && brick.IsSupportedBy(other))
        {
            supportedBy[brick.ID].Add(other.ID);
        }
    }
}

var candidates = orderedBricks
    .Select(b => b.ID)
    .Where(b => supportedBy.Values.All(v => (!v.Contains(b)) || v.Count() > 1))
    .ToList();

Console.WriteLine($"Candidates for removal: {string.Join(',', candidates)}, #={candidates.Count()}");

Dictionary<int, List<int>> dependentBricks = orderedBricks.ToDictionary(b => b.ID, b => new List<int> {b.ID});
foreach(var kvp in supportedBy.Where(kvp => kvp.Value.Count() == 1))
{
    dependentBricks[kvp.Value.Single()].Add(kvp.Key);
}

Console.WriteLine($"SupportedBy {string.Join('\n', supportedBy.Select(kvp => $"{kvp.Key}=>{string.Join(',', kvp.Value)}"))}");

bool updated = true;
while(updated)
{
    updated = false;
    Console.WriteLine($"total dependent bricks: {dependentBricks.Sum(d => d.Value.Count())}. Max is {dependentBricks.Max(d => d.Value.Count())}");
    Console.WriteLine($"{string.Join('\n', dependentBricks.Select(kvp => $"{kvp.Key}=>{string.Join(',', kvp.Value)}"))}");
    foreach (var id in dependentBricks.Keys)
    {
        var tmp = dependentBricks[id].SelectMany(d => dependentBricks[d]).ToList();
        tmp.AddRange(supportedBy.Where(kvp => kvp.Value.Except(dependentBricks[id]).Count() == 0).Select(kvp => kvp.Key));
        var additionalDependents = tmp.Except(dependentBricks[id]).ToList();
        updated |= additionalDependents.Any();
        dependentBricks[id].AddRange(additionalDependents);
    }
}

var bestCandidate = dependentBricks.OrderBy(d => d.Value.Count()).Last().Key;
Console.WriteLine($"Best candidate is {bestCandidate} with #destroyed={dependentBricks[bestCandidate].Count()}");

record Point(int[] Coordinates)
{
    public Point(int x, int y, int z)
        : this([x, y, z])
    { }

    public static Point FromLine(string line) => new (
        line.Split(',')
        .Select(s => int.Parse(s))
        .ToArray());

    public int X => Coordinates[0];
    public int Y => Coordinates[1];
    public int Z => Coordinates[2];
    public void OffsetBy(int x, int y, int z)
    {
        Coordinates[0] += x;
        Coordinates[1] += y;
        Coordinates[2] += z;
    }

    public override string ToString()
    {
        return $"({X},{Y},{Z})";
    }
}

record Brick(string Line)
{
    private static int Count = 0;
    public int ID = Count++;
    public Point Start = Point.FromLine(Line.Split('~').First());
    public Point End = Point.FromLine(Line.Split('~').Last());
    public int Min(int i) => Math.Min(Start.Coordinates[i], End.Coordinates[i]);
    public int Max(int i) => Math.Max(Start.Coordinates[i], End.Coordinates[i]);

    public void Fall(IList<Brick> otherBricks)
    {
        while (Min(2) != 1 && !otherBricks.Any(IsSupportedBy))
        {
            Start.OffsetBy(0, 0, -1);
            End.OffsetBy(0, 0, -1);
        }
    }

    public bool IsSupportedBy(Brick other)
    {
        if(other.Max(2) + 1 != Min(2))
        {
            return false;
        }
        var aa = Expand().ToList();
        var bb = other.Expand().ToList();
        return aa.Any(a => bb.Any(b => a.X == b.X && a.Y == b.Y));
    }

    private IEnumerable<Point> Expand()
    {
        for(int d = Min(Direction); d <= Max(Direction); ++d)
        {
            int[] c = new int[3];
            Start.Coordinates.CopyTo(c, 0);
            c[Direction] = d;
            yield return new Point(c);
        }
    }

    private int Direction =>
        Start.Z != End.Z ? 2
        : Start.Y != End.Y ? 1
        : 0;
}