// Day1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <fstream>
#include <vector>

const std::vector<size_t> input = { 1721,979,366,299,675,1456 };

size_t crackTheCode(std::vector<size_t> data)
{
    for (auto a = data.cbegin(); a != data.cend(); ++a)
    {
        for (auto b = std::next(a); b != data.cend(); ++b)
        {
            for (auto c = std::next(b); c != data.cend(); ++c)
            {
                //if (*a * *b * *c == 0) continue;
                if (*a + *b + *c == 2020)
                {
                    return *a * *b * *c;
                }
            }
        }
    }
    return 0;
}

int main()
{
    auto inputFile = std::ifstream("C:\\Users\\alexa\\source\\repos\\Day1\\Input.txt");
    std::vector<size_t> data(256);

    size_t value;
    while (!(inputFile >> value).eof())
    {
        data.push_back(value);
    }
    std::cout << crackTheCode(data);
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
