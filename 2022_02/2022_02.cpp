#include <iostream>
#include <fstream>
#include <map>
#include <string>

const auto outcome = std::map<std::string,int>
{
    {"AX", 1}, {"BX", 0}, {"CX", 2},
    {"AY", 2}, {"BY", 1}, {"CY", 0},
    {"AZ", 0}, {"BZ", 2}, {"CZ", 1}
};

int calculateScoreDelta(char playerA, char playerB)
{
    const auto scoreForSymbol = static_cast<int>(playerB) - static_cast<int>('W');
    const auto scoreForResult = 3 * outcome.at({playerA, playerB});
    return scoreForSymbol + scoreForResult;
}

int main()
{
    auto file = std::ifstream("Input.txt");

    int score = 0, score2 = 0;
    char playerA, playerB;
    while(file >> playerA >> playerB)
    {
        score += calculateScoreDelta(playerA, playerB);

        const auto modifiedB = playerB == 'Y' ? 'X' + (playerA - 'A')
            : playerB == 'X' ? 'X' + (playerA + 5 - 'A') % 3
            :  'X' + (playerA + 4 - 'A') % 3;
        score2 += calculateScoreDelta(playerA, modifiedB);

        std::cout << playerA << " " << playerB << " => " << score << " vs. " << score2 << std::endl;
    }

    std::cout << "score: " << score << " vs. " << score2 << std::endl;
}