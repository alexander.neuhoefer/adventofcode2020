#include <iostream>
#include <fstream>
#include <map>
#include <set>
#include <vector>
#include <string>
#include <regex>

int main()
{
    auto inputFile = std::ifstream("Input.txt");
    std::vector<size_t> data;

    size_t value;
    while (!(inputFile >> value).eof())
    {
        data.push_back(value);
    }
    data.push_back(0);

    std::sort(data.begin(), data.end());
    data.push_back(data.back() + 3);
    for (const auto& d : data)
    {
        std::cout << d << ", ";
    }

    size_t d1 = 0;
    size_t d3 = 0;
    for (size_t i = 1; i < data.size(); ++i)
    {
        size_t diff = data.at(i) - data.at(i - 1);
        if (diff == 1)
        {
            ++d1;
        }
        if (diff == 3)
        {
            ++d3;
        }
    }

    std::cout << std::endl << "d1 = " << d1 << ", d3 = " << d3 << ", d1 * d3 = " << d1 * d3 << std::endl;
}