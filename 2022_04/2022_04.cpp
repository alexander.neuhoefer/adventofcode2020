#include <iostream>
#include <fstream>
#include <string>

struct Assignment
{
    int from = 0;
    int to = 0;

    bool contains(Assignment other)
    {
        return from <= other.from && to >= other.to;
    }

    static bool fullOverlap(Assignment a, Assignment b)
    {
        return a.contains(b) || b.contains(a);
    }

    int length()
    {
        return to - from;
    }

    static bool partialOverlap(Assignment a, Assignment b)
    {
        Assignment fullOverlap = {std::max(a.from, b.from), std::min(a.to, b.to)};
        return fullOverlap.length() >= 0;
    }
};

static std::istream& operator>>(std::istream& stream, Assignment& task)
{
    int from, to;
    char minus;
    stream >> from >> minus >> to;
    task = { from, to };
    return stream;
}

int main()
{
    auto file = std::ifstream("Input.txt");

    Assignment taskA, taskB;
    char separator;
    int fullOverlaps = 0, partialOverlaps = 0;
    while (file >> taskA >> separator >> taskB)
    {
        fullOverlaps += static_cast<int>(Assignment::fullOverlap(taskA, taskB));
        partialOverlaps += static_cast<int>(Assignment::partialOverlap(taskA, taskB));
    }

    std::cout << "fullOverlaps: " << fullOverlaps << std::endl;
    std::cout << "partialOverlaps: " << partialOverlaps << std::endl;
}