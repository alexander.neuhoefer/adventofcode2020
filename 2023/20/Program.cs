﻿using System.Numerics;
using Pulse = bool;

var creationParams = File.ReadAllLines("./longInput.txt")
    .Select(l => new CreationParams(l))
    .ToList();

Console.WriteLine($"{string.Join('\n', creationParams)}");

var modules = creationParams.Select(
    p => (IModule)(p.Type switch
    {
        '%' => new FlipFlop() { Name = p.Name },
        '&' => new Conjunction() { Name = p.Name },
        _ => new Broadcaster() { Name = p.Name }
    })).ToDictionary(m => m.GetName(), m => m);

foreach (var p in creationParams)
{
    foreach(var other in p.Connections)
    {
        if(!modules.ContainsKey(other))
        {
            modules.Add(other, new Output(){ Name = other });
        }
        modules[p.Name].AttachTo(modules[other]);
    }
}

var button = new Broadcaster {Name = "button" };
button.AttachTo(modules["roadcaster"]);

BigInteger i = 0;
var output = (Output)modules["rx"];
for(i = 0; output.lowPulses == 0; ++i)
{
    output.lowPulses = 0;
    button.Broadcast(false);
    AbstractModule.ProcessQ();
    if(i % 1000000 == 0)
    {
        Console.WriteLine(i);
    }
}
Console.WriteLine($"{i}: lowPulses on rx: {output.lowPulses}");

interface IModule
{
    public string GetName();
    public abstract void AttachTo(IModule other);
    public abstract void WasAttachedFrom(IModule other);
    public abstract void Broadcast(Pulse pulse);
    public abstract void Receive(IModule sender, Pulse pulse);
}

class AbstractModule : IModule
{
    public string Name = "";
    protected List<IModule> connectedModules = [];

    public virtual void AttachTo(IModule other)
    {
        connectedModules.Add(other);
        other.WasAttachedFrom(this);
    }

    public virtual void WasAttachedFrom(IModule other)
    {
        //just do nothing
    }

    public string GetName() => Name;

    public virtual void Receive(IModule sender, Pulse pulse) => Broadcast(pulse);

    public virtual void Broadcast(Pulse pulse)
    {
        foreach (IModule module in connectedModules)
        {
            SignalQ.Enqueue(() =>
            {
                //Console.WriteLine($"{GetName()} -{AsText(pulse)}-> {module.GetName()}");
                module.Receive(this, pulse);
                return pulse;
            });
        }
    }

    private static string AsText(Pulse b) => b ? "high" : "low";

    public static Queue<Func<Pulse>> SignalQ = [];
    public static void ProcessQ()
    {
        while(SignalQ.Any())
        {
            var action = SignalQ.Dequeue();
        }
    }
}

class Broadcaster : AbstractModule;

class FlipFlop : AbstractModule
{
    private Pulse state = false;

    public override void Receive(IModule sender, Pulse pulse)
    {
        if(!pulse)
        {
            state = !state;
            Broadcast(state);
        }
    }
}

class Conjunction : AbstractModule
{
    private Dictionary<IModule, Pulse> LastPulses = [];

    public override void WasAttachedFrom(IModule other)
    {
        LastPulses.Add(other, false);
    }

    public override void Receive(IModule sender, Pulse pulse)
    {
        LastPulses[sender] = pulse;
        var allHigh = LastPulses.Values.All(p => p == true);
        Broadcast(!allHigh);
    }
}

class Output : AbstractModule
{
    public int lowPulses = 0;
    public override void Receive(IModule sender, Pulse pulse)
    {
        //just do nothing
        if (!pulse) ++lowPulses;
    }
}

record CreationParams(string Text)
{
    public char Type = Text.First();
    public string Name = Text.Split("->").First().Trim().Substring(1);
    public List<string> Connections = Text.Split("->").Last().Replace(" ", "").Split(',').ToList();
}