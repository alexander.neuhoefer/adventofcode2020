#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <numeric>
#include <vector>
#include <map>

struct Item
{
    std::string name = "";
    Item(std::string name) : name{name} {}
    virtual size_t totalSizeInBytes() const { return 0; }
};

static std::map<std::string, std::vector<std::shared_ptr<Item>>> fileSystem{};

struct Directory : public Item
{
    Directory(std::string name) : Item{name} {}

    size_t totalSizeInBytes() const override
    {
        const auto& content = fileSystem.at(name);
        return std::accumulate(content.cbegin(), content.cend(), size_t(0), [](size_t totalSize, const auto& item)
        {
            return totalSize + item->totalSizeInBytes();
        });
    }

    auto operator<=>(const Directory& other) const
    {
        return this->name <=> other.name;
    }
};

struct File : public Item
{
    File(std::string name, size_t sizeInBytes)
        : Item{name}
        , sizeInBytes(sizeInBytes)
    {}

    size_t sizeInBytes = 0;

    size_t totalSizeInBytes() const override
    {
        return sizeInBytes;
    }
};

static bool isCommand(const std::string& line)
{
    return line.at(0) == '$';
}

static std::string absolutePath(const std::vector<std::string>& path)
{
    return std::accumulate(path.cbegin(), path.cend(), std::string(""), [](const std::string& parent, const std::string& dir)
    {
        return parent + "/" + dir;
    });
}

int main()
{
    auto file = std::ifstream("Input.txt");

    std::string line;
    std::vector<std::string> currentPath;
    while (std::getline(file, line))
    {
        std::cout << line << " => ";
        if(isCommand(line))
        {
            if(line.at(2) == 'c')
            {
                auto directory = line.substr(5);
                if(directory == "/")
                {
                    currentPath;
                }
                else if(directory == "..")
                {
                    currentPath.pop_back();
                }
                else
                {
                    currentPath.push_back(directory);
                    fileSystem[absolutePath(currentPath)];
                }

                std::cout << "current directory is " << absolutePath(currentPath) << std::endl;
            }
            else
            {
                std::cout << "ls" << std::endl;
            }
        }
        else
        {
            if(line.at(0) == 'd')
            {
                auto dir = std::make_shared<Directory>(absolutePath(currentPath) + "/" + line.substr(4));
                fileSystem[absolutePath(currentPath)].push_back(dir);
                std::cout << "directory " << dir->name << std::endl;
            }
            else
            {
                std::stringstream stream(line);
                size_t size;
                std::string name;
                stream >> size >> name;
                auto file = std::make_shared<File>(name, size);
                fileSystem[absolutePath(currentPath)].push_back(file);
                std::cout << "file " << file->name << " with size " << file->totalSizeInBytes() << std::endl;
            }
        }
    }

    constexpr size_t threshold = 100000;
    size_t sumOfDirectoriesBelowThreshold = 0;
    for(const auto& [dir, content] : fileSystem)
    {
        if(dir == "/")
        {
            continue;
        }

        const auto size = std::accumulate(content.cbegin(), content.cend(), size_t(0), [](size_t totalSize, const std::shared_ptr<Item>& item)
        {
            return totalSize + item->totalSizeInBytes();
        });
        std::cout << dir << " with size: " << size << std::endl;
        if(size <= threshold)
        {
            sumOfDirectoriesBelowThreshold += size;
        }
    }
    std::cout << "sumOfDirectoriesOverThreshold: " << sumOfDirectoriesBelowThreshold << std::endl;


    const auto& rootContent = fileSystem.at("");
    auto totalSize = std::accumulate(rootContent.cbegin(), rootContent.cend(), size_t(0), [](size_t totalSize, const std::shared_ptr<Item>& item)
    {
        return totalSize + item->totalSizeInBytes();
    });
    const auto spaceToBeFreed = totalSize - (70000000 - 30000000);
    std::cout << "total size: " << totalSize << " space to be freed: " << spaceToBeFreed << ", large enough directories: " << std::endl;
    size_t smallestOne = -1;
    for(const auto& [dir, content] : fileSystem)
    {
        auto totalSize = std::accumulate(content.cbegin(), content.cend(), size_t(0), [](size_t totalSize, const std::shared_ptr<Item>& item)
        {
            return totalSize + item->totalSizeInBytes();
        });
        if(totalSize >= spaceToBeFreed)
        {
            smallestOne = std::min(smallestOne, totalSize);
            std::cout << dir << " with " << totalSize << std::endl;
        }
    }
    std::cout << "Smallest: " << smallestOne << std::endl;
}