#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <set>
#include <map>
#include <vector>
#include <algorithm>

using Hash = std::string;

struct Position
{
    int x = 0;
    int y = 0;

    void move(Position offset)
    {
        *this = {x += offset.x, y += offset.y};
    }

    void adjust(const Position& head)
    {
        int dx = head.x - x;
        int dy = head.y - y;
        if(std::abs(dx) <= 1 && std::abs(dy) <= 1)
        {
            return;
        }

        if(dx == 0)
        {
            y += std::copysign(1, dy);
        }
        else if(dy == 0)
        {
            x += std::copysign(1, dx);
        }
        else
        {
            y += std::copysign(1, dy);
            x += std::copysign(1, dx);
        }
    }

    Hash id()
    {
        std::stringstream hash;
        hash << x << "," << y;
        return hash.str();
    }
};

int main()
{
    auto file = std::ifstream("Input.txt");

    const std::map<char, Position> directionToOffset = {
        { 'L', { -1, 0 } },
        { 'R', { +1, 0 } },
        { 'D', { 0, -1 } },
        { 'U', { 0, 1 } }
    };

    std::set<Hash> visitedLocations;
    char dir;
    int steps;
    std::vector<Position> rope{ size_t(10), Position{0, 0} };
    auto& head = rope.front();
    auto& tail = rope.back();
    while(file >> dir >> steps)
    {
        std::cout << "== " << dir << " " << steps << std::endl;
        const auto offset = directionToOffset.at(dir);
        while(steps--)
        {
            head.move(offset);
            for(int i = 1; i < rope.size(); ++i)
            {
                rope.at(i).adjust(rope.at(i - 1));
            }
            visitedLocations.insert(tail.id());
        }
    }

    std::cout << "Visited locations: " << visitedLocations.size() << std::endl;
}