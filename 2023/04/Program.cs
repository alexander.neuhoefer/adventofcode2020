﻿var input = File.ReadAllLines("./longInput.txt").ToList();
var AsNumbers = (string s) => s.Split(' ').Where(s => !string.IsNullOrWhiteSpace(s))
    .Select(x => int.Parse(x.Trim())).ToList();

int points = 0;
int id = 0;
var count = Enumerable.Repeat(1, input.Count()).ToList();
foreach(var line in input)
{
    var parts = line.Split(new char[] { ':', '|' });
    var (game, winningNumbers, numbers) = (parts[0], parts[1], parts[2]);

    var winners = AsNumbers(winningNumbers).Intersect(AsNumbers(numbers)).ToList();
    points += winners.Any() ? (int)Math.Pow(2, winners.Count() - 1) : 0;

    for(int i = 1; i <= winners.Count(); ++i)
    {
        count[id + i] += count[id];
    }

    Console.WriteLine($"{game}: {winningNumbers} | {numbers} => running points: {points}, copies: {count[id++]}");
}

var numberOfScratchCards = count.Aggregate(0, (s, c) => s + c);
Console.WriteLine($"Total number of scratchcards: {numberOfScratchCards}");