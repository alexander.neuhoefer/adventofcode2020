#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <string>
#include <regex>

using Passport = std::map<std::string, std::string>;

static const std::vector<std::string> requiredKeys{"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"};

static const std::map<std::string, std::regex> validationRegex{
    {"byr", std::regex("^(19[2-9]\\d|200[0-2])$")},
    {"iyr", std::regex("^20(1\\d|20)$")},
    {"eyr", std::regex("^20(2\\d|30)$")},
    {"hgt", std::regex("^(1([5-8]\\d|9[0-3])cm|(59|6\\d|7[0-6])in)$")},
    {"hcl", std::regex("^#[0-9a-f]{6}$")},
    {"ecl", std::regex("^(amb|blu|brn|gry|grn|hzl|oth)$")},
    {"pid", std::regex("^\\d{9}$")}
};

bool isValid(const Passport& passport)
{
    for (const auto& key : requiredKeys)
    {
        if (passport.find(key) == passport.end())
        {
            return false;
        }
        const auto& entry = passport.at(key);
        const auto& regex = validationRegex.at(key);
        if (!std::regex_match(entry, regex))
        {
            return false;
        }
    }
    return true;
}

void parseLine(Passport& passport, const std::string& line)
{
    size_t i = 0;
    while (true)
    {
        auto split = line.find(':', i);
        if (split == std::string::npos)
        {
            return;
        }
        auto end = line.find(' ', i);
        if (end == std::string::npos)
        {
            end = line.size();
        }
        passport.emplace(line.substr(i, split - i), line.substr(split + 1, end - (split + 1)));
        i = end + 1;
    }
}

int main()
{
    auto inputFile = std::ifstream("C:\\Users\\alexa\\source\\repos\\Day4\\Input.txt");
    Passport passport;

    std::string line;
    int validPassports = 0;
    while (std::getline(inputFile, line))
    {
        std::cout << "_" << line << std::endl;
        if (line.empty())
        {
            validPassports += isValid(passport) ? 1 : 0;
            passport.clear();
        }
        else
        {
            parseLine(passport, line);
        }
    }
    validPassports += isValid(passport) ? 1 : 0;

    std::cout << validPassports;
}