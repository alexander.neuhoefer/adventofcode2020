﻿var input = File.ReadAllLines("./longInput.txt").ToList();
var height = input.Count();
var width = input.First().Count();
var grid = input.SelectMany(s => s.ToCharArray()).ToArray();

var TiltWest = () => Tilt(ref grid);
var TiltEast = () => Tilt(ref grid, false);
var TiltSouth = () => Tilt(ref grid, false, true);
var TiltNorth = () => Tilt(ref grid, true, true);

var Cycle = (int n = 1) =>
{
    var map = new Dictionary<int, int>();
    var stepsTillTargetRemainder = 0;
    for (int i = 0; i < n; ++i)
    {
        TiltNorth();
        TiltWest();
        TiltSouth();
        TiltEast();
        var hash = CalculateLoadOnNorthBeams(grid);
        if (map.ContainsKey(hash))
        {
            var cycleStart = map[hash];
            var cycleLength = i - cycleStart;
            if(cycleLength < 1000)
            {
                continue;
            }
            Console.WriteLine($"Cycle detected. Starts at {cycleStart} with length {cycleLength}.");
            var targetRemainder = n % cycleLength;
            stepsTillTargetRemainder = (i + cycleLength - targetRemainder) % cycleLength;
            Console.WriteLine($"Remaining steps: {stepsTillTargetRemainder}");
            break;
            //99530
        }
        else
        {
            map.Add(hash, i);
        }
    }
    for (int i = 0; i < stepsTillTargetRemainder; ++i)
    {
        TiltNorth();
        TiltWest();
        TiltSouth();
        TiltEast();
    }
};

Visualize(grid);
CalculateLoadOnNorthBeams(grid);
Cycle(1000000000);
CalculateLoadOnNorthBeams(grid);

void Transpose(ref char[] grid)
{
    var result = new char[grid.Length];
    for (int y = 0; y < height; ++y)
    {
        for (int x = 0; x < width; ++x)
        {
            result[x * height + y] = grid[y * width + x];
        }
    }
    grid = result;
}

void Tilt(ref char[] grid, bool descending = true, bool transpose = false)
{
    var MergeBuffer = (List<char> r, List<char> b) =>
    {
        r.AddRange(descending ? b.OrderDescending() : b.Order());
        b.Clear();
    };

    var length = width;
    if(transpose)
    {
        Transpose(ref grid);
        length = height;
    }

    int i = 0;
    foreach (var chunk in grid.Chunk(length))
    {
        var result = new List<char>();
        var buffer = new List<char>();
        foreach (var c in chunk)
        {
            switch(c)
            {
                case '#':
                    MergeBuffer(result, buffer);
                    result.Add(c);
                    break;
                default:
                    buffer.Add(c);
                    break;
            }
        }
        MergeBuffer(result, buffer);
        result.CopyTo(grid, i);
        i += length;
    }

    if (transpose)
    {
        Transpose(ref grid);
    }
}

int CalculateLoadOnNorthBeams(char[] grid)
{
    var sum = 0;
    int y = height;
    foreach(var row in grid.Chunk(width))
    {
        for (int i = 0; i < row.Count(); ++i)
        {
            var c = row[i];
            if (c == 'O')
            {
                sum += y;
            }
        }
        --y;
    }

    Console.WriteLine($"Total load on north beams is: {sum}");
    return sum;
}

void Visualize(char[] c)
{
    foreach(var row in c.Chunk(width))
    {
        Console.WriteLine(row);
    }
    Console.WriteLine(new string('-', 10));
}