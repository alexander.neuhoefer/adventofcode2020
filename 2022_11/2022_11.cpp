#include <iostream>
#include <fstream>
#include <string>
#include <deque>
#include <map>
#include <sstream>
#include <algorithm>
#include <functional>

static constexpr int N = 30;

struct MOD
{
    int mod[N] = {0};

    MOD(int value)
    {
        for(int i = 0; i < N; ++i)
        {
            mod[i] = value % (i+1);
        }
    }

    int operator%(int a)
    {
        return mod[a-1];
    }
};

MOD operator+(const MOD& a, const MOD& b)
{
    MOD result(0);
    for(int i = 0; i < N; ++i)
    {
        result.mod[i] = (a.mod[i] + b.mod[i]) % (i+1);
    }
    return result;
}

MOD operator*(const MOD& a, const MOD& b)
{
    MOD result(0);
    for(int i = 0; i < N; ++i)
    {
        result.mod[i] = (a.mod[i] * b.mod[i]) % (i+1);
    }
    return result;
}

using Item=MOD;
using Index=int;

struct Monkey
{
    std::deque<Item> items = {};
    std::function<void(Item&)> inspect;
    std::function<Index(Item)> test;

    void act();
    size_t numberOfInspectedItems = 0;
};

static std::deque<Monkey> monkeys = {};

void Monkey::act()
{
    numberOfInspectedItems += items.size();
    while(!items.empty())
    {
        auto currentItem = items.front();
        items.pop_front();

        inspect(currentItem);
        //currentItem /= 3;

        auto& targetMonkey = monkeys.at(test(currentItem));
        targetMonkey.items.push_back(currentItem);
    }
}

static std::string line;

std::istream& skipLine(std::istream& stream)
{
    return std::getline(stream, line);
}

std::deque<Item> parseStartingItems(std::istream& stream)
{
    std::getline(stream, line);
    line = line.substr(line.find(':') + 1);

    std::replace(line.begin(), line.end(), ',', ' ');
    std::cout << line << std::endl;
    std::stringstream sstream(line);
    std::deque<Item> items;
    int item;
    std::cout << "items: ";
    while(sstream >> item)
    {
        items.push_back(item);
        std::cout << item << " ";
    }
    std::cout << std::endl;
    return items;
}

Item conv(const std::string& text, Item old)
{
    if(text == "old")
    {
        return old;
    }
    return std::atoi(text.c_str());
}

std::function<void(Item&)> parseInspectFunction(std::istream& stream)
{
    constexpr char text[] = "  Operation:";
    stream.ignore(std::size(text));

    //new = old + 6
    std::string _, a, b;
    char op;
    stream >> _ >> _ >> a >> op >> b;

    static const std::map<char, std::function<Item(const Item&, const Item&)>> operations = {
        {'+', std::plus<Item>()},
        {'*', std::multiplies<Item>()}
    };
    std::cout << "inspect: " << a << " " << op << " " << b << std::endl;

    auto& f = operations.at(op);
    return [=](Item& item){
        item = f(conv(a, item), conv(b, item));
    };
}

std::function<Index(Item)> parseTestFunction(std::istream& stream)
{
    int mod, target1, target2;

    constexpr char divisibleBy[] = "  Test: divisible by";
    stream.ignore(std::size(divisibleBy));
    stream >> mod;

    constexpr char throwTrue[] = "    If true: throw to monkey";
    stream.ignore(std::size(throwTrue));
    stream >> target1;

    constexpr char throwFalse[] = "    If false: throw to monkey";
    stream.ignore(std::size(throwFalse));
    stream >> target2;
    std::cout << "test: " << mod << " ? " << target1 << " : " << target2 << std::endl;
    std::getline(stream, line); //remove the remainder of the line

    return [=](Item item){
        return item % mod == 0 ? target1 : target2;
    };
}

void printItems()
{
    for(const auto& monkey : monkeys)
    {
        std::cout << "items: ";
        for(const auto& i : monkey.items)
        {
            std::cout << i.mod[0] << " ";
        }
        std::cout << std::endl;
    }
}

int main()
{
    auto file = std::ifstream("Input.txt");
    std::string dummy, startingItems, op, test, pass, fail;
    while(std::getline(file, line))
    {
        monkeys.push_back({
            parseStartingItems(file),
            parseInspectFunction(file),
            parseTestFunction(file)
        });
        std::getline(file, line);
        std::cout << std::endl;
    }
    std::cout << "Done parsing..." << std::endl;

    constexpr int NUMBER_OF_ROUNDS = 10000;
    for(int i = 0; i < NUMBER_OF_ROUNDS; ++i)
    {
        std::cout << "After round " << i + 1 << std::endl;
        for(auto& monkey : monkeys)
        {
            monkey.act();
        }
        //printItems();
    }

    for(const auto& monkey : monkeys)
    {
        std::cout << "Number of inspected items is " << monkey.numberOfInspectedItems << std::endl;
    }
    std::sort(monkeys.begin(), monkeys.end(), [](const Monkey& a, const Monkey& b){
        return a.numberOfInspectedItems > b.numberOfInspectedItems;
    });

    std::cout << "Monkey business: " << monkeys[0].numberOfInspectedItems * monkeys[1].numberOfInspectedItems;
}